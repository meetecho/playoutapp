#! /usr/bin/env bash

APPNAME="Playout";

./gradlew clean war;
sudo rm -rfv ../../data/$1/webapps/$APPNAME.war ../../data/$1/webapps/$APPNAME/
sudo cp -v build/libs/*.war ../../data/$1/webapps/$APPNAME.war
