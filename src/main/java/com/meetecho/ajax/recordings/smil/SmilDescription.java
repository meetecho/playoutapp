/***************************************************************************
 *   Copyright (C) 2010 by Lorenzo Miniero (lorenzo@meetecho.com)          *
 *   Meetecho s.r.l. and University of Napoli Federico II                  *
 *   COMICS Research Group (http://www.comics.unina.it)                    *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

package com.meetecho.ajax.recordings.smil;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.regex.Pattern;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class SmilDescription {

	//Helper class to store messages
	public enum MsgType {
		VIDEO,
		PRELOAD,
		VIDEOTC,
		SUB,
		VIDEOSLIDE,
		AUDIO,
		POLL,
		WHITEBOARD,
		LABEL,
		SLIDE,
		CHAT
	}

	public class RecMessage {
		private Long startTime, endTime;
		private MsgType messageType;
		private String attribute, content;

		public RecMessage(Long sT, Long eT, String a, String gName) {
			this.startTime = sT;
			this.endTime = eT;
			this.attribute = a;

			if (gName.equalsIgnoreCase("textstream")) 
				this.messageType = MsgType.CHAT;

			else if (gName.equalsIgnoreCase("subtitles")) 
				this.messageType = MsgType.SUB;

			else if (gName.equalsIgnoreCase("audio"))
				this.messageType = MsgType.AUDIO;

			else if (gName.equalsIgnoreCase("wb"))
				this.messageType = MsgType.WHITEBOARD;

			else if (gName.equalsIgnoreCase("timelabel"))
				this.messageType = MsgType.LABEL;

			else if (gName.equalsIgnoreCase("poll"))
				this.messageType = MsgType.POLL;

			else if (gName.equalsIgnoreCase("img"))
				this.messageType = MsgType.SLIDE;

			else if (gName.equalsIgnoreCase("video"))
				this.messageType = MsgType.VIDEO;

			else if (gName.equalsIgnoreCase("videotc"))
				this.messageType = MsgType.VIDEOTC;
			
			else if (gName.equalsIgnoreCase("videoslide"))
				this.messageType = MsgType.VIDEOSLIDE;

			else if (gName.equalsIgnoreCase("preload"))
				this.messageType = MsgType.PRELOAD;


			this.content = this.getContent();
		}

		public String toString() {
			String idTime = String.format("%07d", this.startTime);
			if (this.messageType==MsgType.CHAT) 
				return "<jabber id=\"CHAT-" + idTime + "\"><![CDATA[" + forXML(this.content) + "]]></jabber>";
			else if (this.messageType==MsgType.VIDEO || this.messageType==MsgType.VIDEOTC || this.messageType==MsgType.VIDEOSLIDE)
				return "<" + this.messageType + " start=\""+Math.floor((this.startTime)/1000)+"\" end=\""+Math.floor((this.endTime)/1000)+"\" id=\""+MsgType.VIDEO+"-"+ idTime +"\">" + this.content + "</"+this.messageType + ">";
			else if (this.messageType==MsgType.LABEL)
				return "<" + this.messageType + " start=\""+Math.floor((this.startTime)/1000)+"\" id=\""+MsgType.LABEL+"-"+ idTime +"\"><![CDATA[" + this.content + "]]></"+this.messageType + ">";				
			else if (this.messageType==MsgType.AUDIO)
				return "<" + this.messageType + " start=\""+Math.floor((this.startTime)/1000)+"\" id=\""+this.messageType+"-"+ idTime +"\">" + this.content + "</"+this.messageType + ">";				
			else if (this.messageType==MsgType.WHITEBOARD && this.startTime<1000)
				return "<" + this.messageType + " init=\""+SmilDescription.pDB.getWBInitID(this.attribute)+"\" id=\""+this.messageType+"-"+ idTime +"\">" + this.content + "</"+this.messageType + ">";				
			else if (this.messageType==MsgType.PRELOAD)
				return "<" + this.messageType + " start=\""+Math.floor((this.startTime)/1000)+"\" id=\""+MsgType.VIDEO+"-"+ idTime +"\">" + this.content + "</"+this.messageType + ">";				
			else if (this.messageType==MsgType.SUB)
				return "<" + this.messageType + " start=\""+Math.floor((this.startTime)/1000)+"\" durate=\""+Math.floor((this.endTime - this.startTime)/1000)+"\" id=\""+this.messageType+"-"+ idTime +"\"><![CDATA[" + this.content + "]]></"+this.messageType + ">";				
			else
				return "<" + this.messageType + " id=\""+this.messageType+"-"+ idTime +"\">" + this.content + "</"+this.messageType + ">";
		}

		protected String getContent() {

			String result = this.attribute;

			if (this.messageType.equals(MsgType.POLL))
				result = SmilDescription.pDB.getPoll(result);
			else if (this.messageType.equals(MsgType.WHITEBOARD) && this.startTime<1000)
				result = SmilDescription.pDB.getWBInit(result);
			else if (this.messageType.equals(MsgType.WHITEBOARD) && this.startTime>=1000)
				result = SmilDescription.pDB.getWBMsg(result);
//			else if (this.messageType.equals(MsgType.LABEL))
//				result = SmilDescription.pDB.getTimeMark(result);
			return result;
		}

		public String getMsgType() {
			return this.messageType.toString();
		}
		public String forXML(String aText) {
			final StringBuilder result = new StringBuilder();
			final StringCharacterIterator iterator = new StringCharacterIterator(
					aText);
			char character = iterator.current();
			while (character != CharacterIterator.DONE) {
				if(character == '<') {
					result.append("&lt;");
				} else if(character == '>') {
					result.append("&gt;");
				} else if(character == '\"') {
					result.append("&quot;");
				} else if(character == '\'') {
					result.append("&#039;");
				} else if(character == '&') {
					result.append("&amp;");
				} else {
					result.append(character);
				}
				character = iterator.next();
			}
			return result.toString();
		}

		public long getStartTime() {
			return this.startTime;
		}

		public long getEndTime() {
			return this.endTime;
		}
	}

	// Helper class to parse XML
	private class MetadataHandler extends DefaultHandler {

		private TreeMap<Long, RecMessage> messages;
		private TreeMap<Long, RecMessage> stopVideoMessage;
		private TreeMap<Long, RecMessage> initMessages;
		private String scannedMsg[] = new String[]{"wb","poll","audio","video","videotc","videoslide","img"}; //,"marktime"};
		private String audio = null;
		private Long startTime = null, endTime = null;

		public void startElement(String uri, String localName, String gName, Attributes attributes) throws SAXException {

			if (this.messages==null)
				this.messages = new TreeMap<Long, RecMessage>();
			
			if (this.stopVideoMessage==null)
				this.stopVideoMessage = new TreeMap<Long, RecMessage>();

			if (this.initMessages==null)
				this.initMessages = new TreeMap<Long, RecMessage>();

			if (gName.equalsIgnoreCase("timelabel")) {
				String attribute = attributes.getValue("src");

				HttpClient httpClient = new DefaultHttpClient();
				String chatUrl = attribute;
				if(SmilManager.getInstance().getLocalIp() != null)
					chatUrl = chatUrl.replace(SmilManager.getInstance().getPublicIp(), SmilManager.getInstance().getLocalIp());
				HttpGet httpGet = new HttpGet(chatUrl);
				HttpResponse reply = null;
				try {
					reply = httpClient.execute(httpGet);
					if(reply.getStatusLine().getStatusCode() == 200) {
						HttpEntity result = reply.getEntity();
						InputStream inputStream = result.getContent();
						Scanner scanner = new Scanner(inputStream, "UTF-8");
						String line = null;
						while(scanner.hasNextLine()) {
							line = scanner.nextLine();
							if(line != null) {
								if(labelMatcher(line)) {
									// FIXME badly
									String parts[] = line.split(" --> ");
									//startime
									String time[] = parts[0].split(":");
									Double startDouble = Double.parseDouble(time[2])*1000 +
											Integer.parseInt(time[1])*60*1000 +
											Integer.parseInt(time[0])*60*60*1000;
									long startLong = startDouble.longValue();
																		
									String label = parts[1];

									this.initMessages.put(startLong*-1, new RecMessage(startLong,startLong,label,gName));
								}
							}
						}
					}
				} catch (ClientProtocolException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}				

			} else if (gName.equalsIgnoreCase("subtitles")) {
				String attribute = attributes.getValue("src");

				HttpClient httpClient = new DefaultHttpClient();
				String chatUrl = attribute;
				if(SmilManager.getInstance().getLocalIp() != null)
					chatUrl = chatUrl.replace(SmilManager.getInstance().getPublicIp(), SmilManager.getInstance().getLocalIp());
				HttpGet httpGet = new HttpGet(chatUrl);
				HttpResponse reply = null;
				try {
					reply = httpClient.execute(httpGet);
					if(reply.getStatusLine().getStatusCode() == 200) {
						HttpEntity result = reply.getEntity();
						InputStream inputStream = result.getContent();
						Scanner scanner = new Scanner(inputStream, "UTF-8");
						String line = null;
						while(scanner.hasNextLine()) {
							line = scanner.nextLine();
							if(line != null) {
								if(subsMatcher(line)) {
									// FIXME badly
									String parts[] = line.split(" --> ");
									//startime
									String time[] = parts[0].split(":");
									Double startDouble = Double.parseDouble(time[2])*1000 +
											Integer.parseInt(time[1])*60*1000 +
											Integer.parseInt(time[0])*60*60*1000;
									long startLong = startDouble.longValue();

									time = parts[1].split(":");
									Double endDouble = Double.parseDouble(time[2])*1000 +
											Integer.parseInt(time[1])*60*1000 +
											Integer.parseInt(time[0])*60*60*1000;
									long endLong = endDouble.longValue();
																		
									String sub = scanner.nextLine();

									Long timepointer = startLong;

									while (this.messages.containsKey(timepointer))
										timepointer++;

									this.messages.put(timepointer, new RecMessage(startLong,endLong,sub,gName));
								}
							}
						}
					}
				} catch (ClientProtocolException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}				
			} else if (gName.equalsIgnoreCase("textstream")) {
				String attribute = attributes.getValue("src");

				HttpClient httpClient = new DefaultHttpClient();
				String chatUrl = attribute;
				if(SmilManager.getInstance().getLocalIp() != null)
					chatUrl = chatUrl.replace(SmilManager.getInstance().getPublicIp(), SmilManager.getInstance().getLocalIp());
				HttpGet httpGet = new HttpGet(chatUrl);
				HttpResponse reply = null;
				try {
					reply = httpClient.execute(httpGet);
					if(reply.getStatusLine().getStatusCode() == 200) {
						HttpEntity result = reply.getEntity();
						InputStream inputStream = result.getContent();
						Scanner scanner = new Scanner(inputStream, "UTF-8");
						String line = null;
						while(scanner.hasNextLine()) {
							line = scanner.nextLine();
							if(line != null) {
								System.out.println("Analyzing: " + line);
								if(line.startsWith("<Time begin=")) {
									// FIXME badly
									System.out.println(" -- Starts with time begin ");
									String parts[] = line.split("<br/>");
									System.out.println(" -- splitted in parts[0] " + parts[0]);
									System.out.println(" -- and parts[1] " + parts[1]);
									String begin[] = parts[0].split("\"");
									System.out.println(" -- calculate time from " + begin[1]);
									String time[] = begin[1].split(":");
									Double ms = Double.parseDouble(time[2])*1000 +
											Integer.parseInt(time[1])*60*1000 +
											Integer.parseInt(time[0])*60*60*1000;
									long mss = ms.longValue();

									Long timepointer = mss;

									while (this.messages.containsKey(timepointer))
										timepointer++;

									if(parts.length == 2) {
										this.messages.put(timepointer, new RecMessage(mss,mss,parts[1],gName));
									} else {
										// Looks like we broke the br
										StringBuffer msg = new StringBuffer();
										for(int i=1; i<parts.length; i++) {
											msg.append(parts[i]);
											if((i+1)<parts.length)
												msg.append("<br/>");
										}
									}
								}
							}
						}
					}
				} catch (ClientProtocolException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}				
			} else if (Arrays.asList(scannedMsg).contains(gName)) {

				if (!this.testFile(attributes.getValue("src")))
					return;

				Long startTime = 0L;
				Long endTime = 0L;
				
				if (attributes.getValue("begin")!=null)
					startTime = Long.parseLong(attributes.getValue("begin").substring(0, attributes.getValue("begin").length()-1))*1000;
				if (attributes.getValue("end")!=null)	
					endTime = Long.parseLong(attributes.getValue("end").substring(0, attributes.getValue("end").length()-1))*1000;
				
				String attribute = attributes.getValue("src");
				TreeMap<Long, RecMessage> tempQueue = this.initMessages;
				Boolean increment = true;
				
				if (startTime>0L) {
					tempQueue = this.messages;
					increment = false;
				}

				if (gName.equalsIgnoreCase("audio"))
					audio = attribute;				

				while(tempQueue.containsKey(startTime))
					if (increment)	++startTime;
					else --startTime;

				tempQueue.put(startTime, new RecMessage(startTime, endTime, attribute, gName));
				
				if (gName.equalsIgnoreCase("video") || gName.equalsIgnoreCase("videotc") || gName.equalsIgnoreCase("videoslide")) {

					endTime = startTime+endTime;
					Long preloadTime = startTime - 5000;
					if (preloadTime <= 0L) {
						while(this.initMessages.containsKey(preloadTime))
							++preloadTime;						
							this.initMessages.put(preloadTime, new RecMessage(startTime, endTime, attribute, "preload"));
					} else {
						while(tempQueue.containsKey(preloadTime))
							++preloadTime;
						this.messages.put(preloadTime, new RecMessage(startTime, endTime, attribute, "preload"));
					}
					
					while(stopVideoMessage.containsKey(endTime))
						++endTime;

					stopVideoMessage.put(endTime, new RecMessage(startTime, endTime, attribute, gName));
				}

				
			} else if (gName.equalsIgnoreCase("start")) {
				startTime = Long.parseLong(attributes.getValue("begin").substring(0, attributes.getValue("begin").length()-1));
			} else if (gName.equalsIgnoreCase("stop")) {
				endTime = Long.parseLong(attributes.getValue("begin").substring(0, attributes.getValue("begin").length()-1));
			} 
		}

		private boolean testFile(String src) {

			if (!src.contains("http"))
				return true;	//sempre true per wb e poll che non hanno url

			URL u = null;
			try {
				u = new URL(src);
				HttpURLConnection huc =  (HttpURLConnection)  u.openConnection();
				huc.setRequestMethod("HEAD");
				huc.connect();
				return (huc.getResponseCode() == HttpURLConnection.HTTP_OK);

			} catch (MalformedURLException e) {
				e.printStackTrace();
				return false;
			} catch (IOException e) {
				e.printStackTrace();
				return false;
			}

		}
	}

	private File smil = null;
	private String content = null;

	//DB
	public static PlayoutDb pDB;


	// Media
	private String audio = null;
	private TreeMap<Long, RecMessage> messages = null;
	private TreeMap<Long, RecMessage> stopVideoMessage = null;
	private TreeMap<Long, RecMessage> initMessage = null;
	private Long startTime, endTime;

	public SmilDescription(File root, String name) throws IllegalArgumentException {
		if(name == null || name.equals(""))
			throw new IllegalArgumentException("Invalid name");
		smil = new File(root, name + ".smil");
		if(!smil.exists())
			throw new IllegalArgumentException("Smil file doesn't exist");
		pDB = PlayoutDb.getInstance();
		try {
			System.out.println("Setting MYSQL DB");
			pDB.setupDB();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public boolean parse() throws IOException, ParserConfigurationException, SAXException {
		FileInputStream fis = new FileInputStream(smil);
		StringBuilder text = new StringBuilder();
		String NL = "\r\n";
		Scanner scanner = new Scanner(fis, "UTF-8");
		while(scanner.hasNextLine()) {
			text.append(scanner.nextLine() + NL);
		}
		scanner.close();
		fis.close();
		// Ok we have the file content
		content = new String(text);
		// Parse XML
		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser saxParser;
		saxParser = factory.newSAXParser();
		ByteArrayInputStream is = new ByteArrayInputStream(content.getBytes());
		MetadataHandler handler = new MetadataHandler();
		saxParser.parse(is, handler);
		is.close();

		if (handler.messages!=null) {
			messages = handler.messages;

			// Videos
			SmilManager sm = SmilManager.getInstance();
			setAudioFile(new String(handler.audio));//.replace(".mov", "." + sm.getWebmExtension()));
			setRecStartTime(handler.startTime);
			setRecEndTime(handler.endTime);
		}

		if (handler.stopVideoMessage!=null) {
			stopVideoMessage = handler.stopVideoMessage;
		}

		if (handler.initMessages!=null) {
			initMessage = handler.initMessages;
		}

		return true;
	}

	public String getContent() {
		return content;
	}

	public TreeMap<Long, RecMessage> getMessages() {
		return this.messages;
	}

	public TreeMap<Long, RecMessage> getInitMessages() {
		return this.initMessage;
	}
	
	public TreeMap<Long, RecMessage> getStopVideoMessage() {
		return this.stopVideoMessage;
	}
	// Test
	public void testLog(TreeMap <Long, RecMessage> tTM) {
		System.out.println("  -- Result");
		Iterator<Long> msIter = tTM.keySet().iterator();
		long ms;
		while(msIter.hasNext()) {
			ms = msIter.next();
			System.out.println("  -- -- " + ms + ": " + tTM.get(ms).toString());
		}
	}

	public String getAudioFile() {
		return audio;
	}

	public void setAudioFile(String audio) {
		this.audio = audio;
	}
	
	public Long getRecStartTime() {
		return this.startTime;
	}

	public void setRecStartTime(Long sT) {
		this.startTime = sT;
	}

	public Long getRecEndTime() {
		return this.endTime;
	}

	public void setRecEndTime(Long eT) {
		this.endTime = eT;
	}

	private static Boolean subsMatcher(String txt) {
		
	    String re1="((?:(?:[0-1][0-9])|(?:[2][0-3])|(?:[0-9])):(?:[0-5][0-9])(?::[0-5][0-9])?(?:\\s?(?:am|AM|pm|PM))?)";	// HourMinuteSec 1
	    String re2=".*?";	// Non-greedy match on filler
	    String re3="(-)";	// Any Single Character 1
	    String re4="(-)";	// Any Single Character 2
	    String re5="(>)";	// Any Single Character 3
	    String re6=".*?";	// Non-greedy match on filler
	    String re7="((?:(?:[0-1][0-9])|(?:[2][0-3])|(?:[0-9])):(?:[0-5][0-9])(?::[0-5][0-9])?(?:\\s?(?:am|AM|pm|PM))?)";	// HourMinuteSec 2
	    Pattern p = Pattern.compile(re1+re2+re3+re4+re5+re6+re7,Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	    java.util.regex.Matcher m = p.matcher(txt);
	    if (m.find())
	    	return true;
		return false;
	}
	
	private static Boolean labelMatcher(String txt) {
		
	    String re1="((?:(?:[0-1][0-9])|(?:[2][0-3])|(?:[0-9])):(?:[0-5][0-9])(?::[0-5][0-9])?(?:\\s?(?:am|AM|pm|PM))?)";	// HourMinuteSec 1
	    String re2=".*?";	// Non-greedy match on filler
	    String re3="(-)";	// Any Single Character 1
	    String re4="(-)";	// Any Single Character 2
	    String re5="(>)";	// Any Single Character 3
	    String re6=".*?";	// Non-greedy match on filler
	    Pattern p = Pattern.compile(re1+re2+re3+re4+re5+re6,Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	    java.util.regex.Matcher m = p.matcher(txt);
	    if (m.find()) {
	    	System.out.println("[LABEL] "+txt+" line matched!");
	    	return true;
	    }
	    	return false;
	}

}
