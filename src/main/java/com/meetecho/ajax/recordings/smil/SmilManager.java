/***************************************************************************
 *   Copyright (C) 2010 by Lorenzo Miniero (lorenzo@meetecho.com)          *
 *   Meetecho s.r.l. and University of Napoli Federico II                  *
 *   COMICS Research Group (http://www.comics.unina.it)                    *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

package com.meetecho.ajax.recordings.smil;

import java.io.File;
import java.io.IOException;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentHashMap;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

public class SmilManager {

	private static SmilManager instance = null;
	
	private File rootdir = null;
	private String redirect = null;
	private String localIp = null, publicIp = null;
	private ConcurrentHashMap<String, SmilDescription> recordings = null;
	
	private boolean h264Enabled = false;
	private String h264Extension = null;
	private boolean webmEnabled = false;
	private String webmExtension = null;
	private boolean ogvEnabled = false;
	private String ogvExtension = null;
	
	private SmilManager() throws IllegalArgumentException {
		System.out.println(" *** SmilManager");
		ResourceBundle resource = ResourceBundle.getBundle("com.meetecho.ajax.recordings.smil.smil");
		if(resource == null)
			throw new IllegalArgumentException("Missing properties");
		String dir = resource.getString("base.dir");
		if(dir == null)
			throw new IllegalArgumentException("Missing dir in properties");
		System.out.println("Root dir for SmilManager is " + dir);
		rootdir = new File(dir);
		if(!rootdir.exists()) {
			System.out.println("  -- Creating directory");
			if(!rootdir.mkdirs())
				throw new IllegalArgumentException("Couldn't create root dir");
		}
		recordings = new ConcurrentHashMap<String, SmilDescription>();
		String temp = resource.getString("h264.enable");
		if(temp.equals("true")) {
			h264Enabled = true;
			h264Extension = new String(resource.getString("h264.ext"));
		}
		temp = resource.getString("webm.enable");
		if(temp.equals("true")) {
			webmEnabled = true;
			webmExtension = new String(resource.getString("webm.ext"));
		}
		temp = resource.getString("ogv.enable");
		if(temp.equals("true")) {
			ogvEnabled = true;
			ogvExtension = new String(resource.getString("ogv.ext"));
		}
		if (resource.containsKey("redirect.url")) {
			redirect = resource.getString("redirect.url");
		}
		if (resource.containsKey("ip.local")) {
			localIp = resource.getString("ip.local");
			System.out.println("  --> ip.local: " + localIp);
		}
		if (resource.containsKey("ip.public")) {
			publicIp = resource.getString("ip.public");
			System.out.println("  --> ip.public: " + publicIp);
		}
	}
	
	public static SmilManager getInstance() {
		if(instance == null) {
			try {
				instance = new SmilManager();
			} catch(IllegalArgumentException e) {
				e.printStackTrace();
				return null;
			}
		}
		return instance;
	}
	
	public boolean recordingExists(String recording) {
		if(recording == null || recording.equals(""))
			return false;
		File rec = new File(rootdir, recording + ".smil");
		if(rec.exists())
			return true;
		return false;
	}

	public boolean recordingExists(String recording, String chapter) {
		if(recording == null || recording.equals(""))
			return false;
		if(chapter == null || chapter.equals(""))
			return false;
		File rec = new File(rootdir, recording + "-" + chapter + ".smil");
		if(rec.exists())
			return true;
		return false;
	}
	
	public SmilDescription getSmilDescription(String recording) {
		if(!recordingExists(recording))
			return null;
		if(recordings.contains(recording))
			return recordings.get(recording);
		SmilDescription sd = new SmilDescription(rootdir, recording);
		try {
			if(!sd.parse())
				return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
			return null;
		} catch (SAXException e) {
			e.printStackTrace();
			return null;
		}
		recordings.put(recording, sd);
		return sd;
	}

	public SmilDescription getSmilDescription(String recording, String chapter) {
		if(!recordingExists(recording, chapter))
			return null;
		String name = recording + "-" + chapter;
		if(recordings.contains(name))
			return recordings.get(name);
		SmilDescription sd = new SmilDescription(rootdir, name);
		try {
			if(!sd.parse())
				return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
			return null;
		} catch (SAXException e) {
			e.printStackTrace();
			return null;
		}
		recordings.put(name, sd);
		return sd;
	}

	public boolean isH264Enabled() {
		return h264Enabled;
	}

	public String getH264Extension() {
		return h264Extension;
	}

	public boolean isWebmEnabled() {
		return webmEnabled;
	}

	public String getWebmExtension() {
		return webmExtension;
	}

	public boolean isOgvEnabled() {
		return ogvEnabled;
	}

	public String getOgvExtension() {
		return ogvExtension;
	}

	public String getRedirectUrl() {
		return redirect;
	}

	public File getRootdir() {
		return rootdir;
	}

	public String getRedirect() {
		return redirect;
	}

	public String getLocalIp() {
		return localIp;
	}

	public String getPublicIp() {
		return publicIp;
	}

	public ConcurrentHashMap<String, SmilDescription> getRecordings() {
		return recordings;
	}
}
