package com.meetecho.ajax.recordings.smil;

import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import com.mchange.v2.c3p0.ComboPooledDataSource;


public class PlayoutDb {

	private static PlayoutDb instance = null;

	private boolean sqlite = false;
	private boolean mysql = false;
	
	private static ComboPooledDataSource cpds = new ComboPooledDataSource();
	private String db = null;
	private Connection conn = null;
	
	private String mysqlServer = null;
	private String mysqlUser = null;
	private String mysqlPass = null;
	

	public enum DbError {
		INVALID_ARGUMENT,
		SQL_ERROR,
		PLUGIN_ERROR,
		NO_SUCH_RECORD,
		OK
	}

	private PlayoutDb() {
		// Private constructor
		ResourceBundle resource = ResourceBundle.getBundle("com.meetecho.ajax.recordings.smil.smil");
		if(resource == null)
			System.out.println("Null resource??");

		// Mysql stuff
		if (resource.containsKey("db.mysql.rec"))
			mysqlServer = resource.getString("db.mysql.rec");
		else 
			mysqlServer = resource.getString("db.mysql");
		
		if (resource.containsKey("db.mysql.user"))
			mysqlUser = resource.getString("db.mysql.user");
		if (resource.containsKey("db.mysql.pass"))
			mysqlPass = resource.getString("db.mysql.pass");
		
	}

	public static PlayoutDb getInstance() {
		if (instance == null) {
			System.out.println("Creating singleton instance...");
			instance = new PlayoutDb();
		}
		return instance;
	}

	// SQLite
	public void setupDB(String db) throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
		synchronized (instance) {
			if (conn != null)
				return;
			mysql = false;
			sqlite = true;
			System.out.println("Setup: SQLite");
			System.out.println("Using " + db + " for Playout");
			this.db = db;
			Class.forName("org.sqlite.JDBC");
			conn = DriverManager.getConnection("jdbc:sqlite:" + db);
			Statement stat = conn.createStatement();
			// Create DB if it doesn't exist
			try {
				stat.executeQuery("select * from Playout;"); 
				System.out.println("Playout DB exists");
			} catch (SQLException e) {
				System.out.println("Playout DB does not exist, creating it");
				stat.executeUpdate("drop table if exists Playout;");
				stat.executeUpdate("create table Playout (RECID INTEGER PRIMARY KEY AUTOINCREMENT, MAKER not null, STARTIME not null, ENDTIME not null, ROOM not null, PATHJOURNAL not null, PATHPOLLMANAGER not null);");
			}
			
			stat.close();
		}
	}
	
	public void setupDB() throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
		synchronized (instance) {
			if (conn != null)
				return;
			mysql = true;
			sqlite = false;
			System.out.println("Setup: MYSQL");
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			//creating connection pool
	        try {
	             cpds=new ComboPooledDataSource();
	             cpds.setDriverClass("com.mysql.jdbc.Driver");
	             cpds.setJdbcUrl(mysqlServer);
	             cpds.setUser(mysqlUser);
	             cpds.setPassword(mysqlPass);
	             cpds.setMaxPoolSize(50);
	             cpds.setMinPoolSize(5);
//	             cpds.setAcquireIncrement(Accomodation);
	         } catch (PropertyVetoException ex) {
	        	 ex.printStackTrace();
	         }					
			
			conn = cpds.getConnection();
//			conn = DriverManager.getConnection(mysqlServer, mysqlUser, mysqlPass);
			Statement stat = conn.createStatement();
			if (conn.isValid(1))
				conn.close();
		}
	}

	public String getDb() {
		return db;
	}

	public Connection getConn() throws SQLException {
		return cpds.getConnection();
	}

	public void close() {
		synchronized (instance) {
			if (conn == null)
				return;
			System.out.println("Closing PlayoutDb Connection");
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			conn = null;
		}
	}

	public String getPoll(String id) {
		if (id == null || id.equals(""))
			return null;

        System.out.println("Queryng poll for "+id);

        String result = null;
		synchronized (instance) {
			try {
				if (conn.isClosed()) conn = cpds.getConnection();
				PreparedStatement prep = conn.prepareStatement("select MSG from PollRec where POLLID=?;");
				prep.setString(1, id);
				ResultSet rs = prep.executeQuery();
				if (!rs.next()) {
					System.out.println("No Poll fot ID "+id);
					rs.close();
					prep.close();
					return null;
				}					
				result = rs.getString("MSG");	
				prep.close();
				if (conn!=null)
					conn.close();
                System.out.println("POLL GET RES: "+result);
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
		}
		return result;
	}

	public String getWBInit(String id) {
		if (id == null || id.equals(""))
			return null;
		
		String result = null;
		synchronized (instance) {
			try {
				if (conn.isClosed()) conn = cpds.getConnection();
				PreparedStatement prep = conn.prepareStatement("select INITSTATE from WbRec where WBID=?;");
				prep.setString(1, id);
				ResultSet rs = prep.executeQuery();
				if (!rs.next()) {
					System.out.println("No WBInit fot ID "+id);
					rs.close();
					prep.close();
					return null;
				}					
				result = rs.getString("INITSTATE");	
				prep.close();
				if (conn!=null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
		}
		return result;
	}

	public String getWBMsg(String id) {
		if (id == null || id.equals(""))
			return null;
		
		String result = null;
		synchronized (instance) {
			try {
				if (conn.isClosed()) conn = cpds.getConnection();
				PreparedStatement prep = conn.prepareStatement("select MSG from WbMsg where MSGID=?;");
				prep.setString(1, id);
				ResultSet rs = prep.executeQuery();
				if (!rs.next()) {
					System.out.println("No WBMsg for ID "+id);
					rs.close();
					prep.close();
					return null;
				}					
				result = rs.getString("MSG");	
				prep.close();
				if (conn!=null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
		}
		return result;
	}

	public String getTimeMark(String id) {
		if (id == null || id.equals(""))
			return null;
		
		String result = null;
		synchronized (instance) {
			try {
				if (conn.isClosed()) conn = cpds.getConnection();
				PreparedStatement prep = conn.prepareStatement("select LABEL from MarkTime where MARKID=?;");
				prep.setString(1, id);
				ResultSet rs = prep.executeQuery();
				if (!rs.next()) {
					System.out.println("No Label for ID "+id);
					rs.close();
					prep.close();
					return null;
				}					
				result = rs.getString("LABEL");	
				prep.close();
				if (conn!=null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
		}
		return result;
	}

	
	public String getWBInitID(String id) {
		if (id == null || id.equals(""))
			return null;
		
		String result = null;
		synchronized (instance) {
			try {
				if (conn.isClosed()) conn = cpds.getConnection();
				PreparedStatement prep = conn.prepareStatement("select WBINDEX from WbRec where WBID=?;");
				prep.setString(1, id);
				ResultSet rs = prep.executeQuery();
				if (!rs.next()) {
					System.out.println("No WB for ID "+id);
					rs.close();
					prep.close();
					return null;
				}					
				result = rs.getString("WBINDEX");	
				prep.close();
				if (conn!=null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
		}
		return result;
	}

}
