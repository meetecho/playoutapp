/***************************************************************************
 *   Copyright (C) 2010 by Lorenzo Miniero (lorenzo@meetecho.com)          *
 *   Meetecho s.r.l. and University of Napoli Federico II                  *
 *   COMICS Research Group (http://www.comics.unina.it)                    *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

package com.meetecho.ajax.recordings.smil;

import java.util.Map.Entry;
import java.util.concurrent.ArrayBlockingQueue;

import com.meetecho.ajax.recordings.smil.SmilDescription.MsgType;
import com.meetecho.ajax.recordings.smil.SmilDescription.RecMessage;

public class PlayoutContext {

	private String id = null;
	public ArrayBlockingQueue<String> messages = null;
	private SmilDescription sd = null;

	Thread thread = null;

	private boolean working = false;
	private boolean playing = false;

	private long current = 1;

	private long timeout = 0;

	public PlayoutContext(String id) {
		this.id = new String(id);
		System.out.println("Creating new PlayoutContext: " + id);
		messages = new ArrayBlockingQueue<String>(1000); // FIXME
		synchronized(messages) {
			playing = false;
			current = 0;
			messages.add("<status id=\"" + id + "\">setup</status>");
		}
	}

	public String getId() {
		return id;
	}

	public void resetTimeout() {
		timeout = System.currentTimeMillis();
	}

	public long getTimeout() {
		return timeout;
	}

	public void setSmilDescription(SmilDescription description) {

		if(sd != null)
			return;
		sd  = description;

		//init
		synchronized(messages) {
			StringBuffer buf = new StringBuffer();			
			for (Entry<Long, RecMessage> entry  : sd.getInitMessages().entrySet())
				buf.append(entry.getValue().toString());
			messages.add(buf.toString());
			buf.delete(0, buf.length());
			messages.add("<status id=\"" + this.id + "\">configured</status>");
		}


		thread = new Thread() {
			public void run() {

				working = true;
				long last = 0;
				Long[] msg = (Long[]) sd.getMessages().keySet().toArray(new Long[sd.getMessages().keySet().size()]);
				int msgIndex = 0;
				long nextMsgMs = msg[0];

				StringBuffer buf = new StringBuffer();
				while(working) {
					try {
						Thread.sleep(200);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					if(!playing) {
						if(current == 0) {
							// Reset
							msgIndex = 0;
							nextMsgMs = msg[0];
							last = 0;
						}
						continue;
					}

					synchronized(messages) {
						if(current > last) {
							// Add something?
							if(nextMsgMs == 0)
								nextMsgMs = msg[0];	// FIXME
							if(current > nextMsgMs) {
								RecMessage msgQueue = null;
								RecMessage msgSlide = null;
								RecMessage msgPoll = null;
								while(current > nextMsgMs) {
									System.out.println("[Msg]  " + current + " > " + nextMsgMs + ": " + sd.getMessages().get(nextMsgMs) + " ---- " + sd.getMessages().get(nextMsgMs).getMsgType());
									msgQueue = sd.getMessages().get(nextMsgMs);

									if (!msgQueue.getMsgType().equalsIgnoreCase(MsgType.POLL.toString()) && !msgQueue.getMsgType().equalsIgnoreCase(MsgType.SLIDE.toString()))
										buf.append(msgQueue.toString());
									else if (msgQueue.getMsgType().equalsIgnoreCase(MsgType.POLL.toString()))
										msgPoll = msgQueue;
									else if (msgQueue.getMsgType().equalsIgnoreCase(MsgType.SLIDE.toString()))
										msgSlide = msgQueue;

									if((msgIndex+1) == msg.length) {
										nextMsgMs = 1000000000;	// FIXME
									} else {
										msgIndex++;
										nextMsgMs = msg[msgIndex];
									}
								}
								if (msgPoll!=null)
									buf.append(msgPoll.toString());
								if (msgSlide!=null)
									buf.append(msgSlide.toString());

							}
						} else if(current < last) {
							// Remove something?
							if(nextMsgMs == 1000000000)
								nextMsgMs = msg[msg.length-1];	// FIXME

							if(current < nextMsgMs) {
								String tempPoll = null, tempWB = null, tempSub = null;

								while(current < nextMsgMs) {
									RecMessage thisMsg = sd.getMessages().get(nextMsgMs);
									System.out.println("[REMOVE]  " + current + " < " + nextMsgMs + ": " + thisMsg.getMsgType());
									if (!thisMsg.getMsgType().equalsIgnoreCase(MsgType.WHITEBOARD.toString()))
										buf.append("<remove>"+thisMsg.getMsgType()+"-" + String.format("%07d", nextMsgMs) + "</remove>");
									else {
										if (getShapeID(thisMsg.getContent())!=null) //E' un disegno
											System.out.println("<remove shapeID=\""+getShapeID(thisMsg.getContent())+"\" wbID=\""+getWbID(thisMsg.getContent())+"\">"+thisMsg.getMsgType()+"-" + nextMsgMs + "</remove>");
										else //E' una create
											System.out.println("<remove>"+thisMsg.getMsgType()+"-" + nextMsgMs + "</remove>");
										
										if (getShapeID(thisMsg.getContent())!=null) //E' un disegno
											buf.append("<remove shapeID=\""+getShapeID(thisMsg.getContent())+"\" wbID=\""+getWbID(thisMsg.getContent())+"\">"+thisMsg.getMsgType()+"-" + nextMsgMs + "</remove>");
										else //E' una create
											buf.append("<remove>"+thisMsg.getMsgType()+"-" + nextMsgMs + "</remove>");
									}
									//FIXME Complete
									if (thisMsg.getMsgType().equalsIgnoreCase(MsgType.POLL.toString()))
										tempPoll = findLastPoll(msg,msgIndex);

									//FIXME Complete
									tempSub = findLastSub(msg,msgIndex,current);

									if(msgIndex == 0) {
										nextMsgMs = 0;	// FIXME
									} else {
										msgIndex--;
										nextMsgMs = msg[msgIndex];
									}
								}
								if (tempPoll!=null)
									buf.append(tempPoll);

								if (tempSub!=null )
									buf.append(tempSub);

								findVideoActive(current,buf, last);

							}
						}

						if(buf.length() > 0) {
							messages.add(buf.toString());
							buf.delete(0, buf.length());
						}
						last = current;

					} 

				}
			}

			private void findVideoActive(long current, StringBuffer buf, long last) {
				for (Entry<Long, RecMessage> entry  : sd.getStopVideoMessage().entrySet()) {
					if (entry.getKey()>current && entry.getValue().getStartTime()<current && entry.getKey()<last)
						buf.append(entry.getValue().toString());
				}
			}

			private String getShapeID(String content) {
				String pktID = null;
				if (content.split("<wbPacketID>").length>1) {
					String temp = content.split("<wbPacketID>")[1];
					pktID = temp.split("</wbPacketID>")[0];
				}
				return pktID;
			}

			private String getWbID(String content) {
				String wbIndex = null;
				if (content.split("<wboardID>").length>1) {
					String temp = content.split("<wboardID>")[1];
					wbIndex = temp.split("</wboardID>")[0];
				}
				return wbIndex;
			}
		};
		thread.start();
	}

	public void play() {
		playing = true;
		synchronized(messages) {
			messages.add("<status>playing</status>");
		}
	}

	public void pause() {
		playing = false;
		synchronized(messages) {
			messages.add("<status>paused</status>");
		}		
	}

	public void seek(long ms) {
		synchronized(messages) {
			current = ms;
		}
	}

	public void reset() {
		synchronized(messages) {
			playing = false;
			current = 0;
			StringBuffer buf = new StringBuffer();			
			for (Entry<Long, RecMessage> entry  : sd.getInitMessages().entrySet())
				buf.append(entry.getValue().toString());
			messages.add(buf.toString());
			buf.delete(0, buf.length());
			messages.add("<status>reset</status>");
		}
		messages.add("<status id=\"" + this.id + "\">configured</status>");

		alert("Playout stopped", "The recording has ended: press play if you want to watch it again, or <a href=\"#\" onclick=\"javascript:history.back(); return false;\">go back to the page you come from</a> to watch another one.");
	}

	public void stop() {
		//		System.out.println("Stopping");
		working = false;
	}


	public void alert(String title, String content) {
		if(messages == null)
			return;
		StringBuffer alert = new StringBuffer();
		alert.append("<alert>");
		alert.append("<title><![CDATA[" + title + "]]></title>");
		alert.append("<content><![CDATA[" + content + "]]></content>");
		//		alert.append("<height>" + content.length() + "</height>");	// FIXME
		alert.append("</alert>");
		synchronized(messages) {
			messages.add(alert.toString());
		}
	}

	private String findLastSub(Long[] msg, int msgIndex, long current) {
		Long nextMsgMs = msg[--msgIndex];
		System.out.println("Finding last sub...");
		while(msgIndex > 0) {
			msgIndex--;
			nextMsgMs = msg[msgIndex];
			System.out.println("   --> "+nextMsgMs+" is "+sd.getMessages().get(nextMsgMs).getMsgType() + " - ends at "+sd.getMessages().get(nextMsgMs).getEndTime() + " and now is " + current);
			if (sd.getMessages().get(nextMsgMs).getMsgType().equalsIgnoreCase(MsgType.SUB.toString()) && sd.getMessages().get(nextMsgMs).getEndTime()>current) {
				return sd.getMessages().get(nextMsgMs).toString();
			}
		}
		return null;
	}
	
	private String findLastPoll(Long[] msg, int msgIndex) {
		Long nextMsgMs = msg[--msgIndex];
		System.out.println("Finding last poll...");
		while(msgIndex > 0) {
			msgIndex--;
			nextMsgMs = msg[msgIndex];
			System.out.println("   --> "+nextMsgMs+" is "+sd.getMessages().get(nextMsgMs).getMsgType());
			if (sd.getMessages().get(nextMsgMs).getMsgType().equalsIgnoreCase(MsgType.POLL.toString())) {
				return sd.getMessages().get(nextMsgMs).toString();
			}
		}
		return null;
	}
}
