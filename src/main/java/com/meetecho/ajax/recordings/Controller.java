/***************************************************************************
 *   Copyright (C) 2010 by Lorenzo Miniero (lorenzo@meetecho.com)          *
 *   Meetecho s.r.l. and University of Napoli Federico II                  *
 *   COMICS Research Group (http://www.comics.unina.it)                    *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

package com.meetecho.ajax.recordings;
import java.io.IOException;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.meetecho.ajax.recordings.smil.PlayoutContext;
import com.meetecho.ajax.recordings.smil.SmilDescription;
import com.meetecho.ajax.recordings.smil.SmilManager;


public class Controller extends HttpServlet implements Runnable {

	private static final long serialVersionUID = 1L;
	private HashMap<String, PlayoutContext> contexts = new HashMap<String, PlayoutContext>();

	// Cleanup thread
	private Thread thread = null;

	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		System.out.println(" *** Controller");

		thread = new Thread(this);
		thread.start();
	}

	@Override
	public void destroy() {
		System.out.println(" *** Controller destroyed");
		contexts = null;	// FIXME
		super.destroy();
	}

	// Helper to generate ids
	static public String generate(int len) {
		if(len < 1)
			return null;
		StringBuffer sb = new StringBuffer(len);  
		int c = 'A';
		int r1 = 0;
		for (int i = 0; i < len; i++) {
			r1 = (int) (Math.random() * 3);  
			switch (r1) {
			case 0:
				c = '0' + (int)(Math.random() * 10);  
				break;
			case 1:  
				c = 'a' + (int)(Math.random() * 26);  
				break;
			case 2:
				c = 'A' + (int)(Math.random() * 26);  
				break;
			default:
				break;
			}
			sb.append((char)c);  
		}
		return sb.toString();
	}

	private void sendErrorResponse(HttpServletResponse response, String reason) throws IOException {
		response.setContentType("application/xml");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		response.getWriter().write("<meetechoweb>");
		response.getWriter().write("<error>" + reason + "</error>");
		response.getWriter().write("</meetechoweb>");
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String action = req.getParameter("action");
		if(action == null || action.equals("")) {
			sendErrorResponse(resp, "Missing action");
			return;
		}
		if(action.equals("getUpdate")) {
			PlayoutContext pc = null;
			String first = req.getParameter("first");
			if(first != null && first.equals("true")) {
				// First update request: setup new playout context
				String recording = req.getParameter("recording");
				if(recording == null || recording.equals("")) {
					sendErrorResponse(resp, "Missing recording");
					return;
				}
				String id = generate(12);
				SmilDescription sd = SmilManager.getInstance().getSmilDescription(recording);
				pc = new PlayoutContext(id);
				if(sd == null) {
					sendErrorResponse(resp, "No such recording");
					return;
				}
				sd.testLog(sd.getMessages());
				pc.setSmilDescription(sd);
				synchronized(contexts) {
					contexts.put(id, pc);
				}
				Enumeration headers = req.getHeaderNames();
				while(headers.hasMoreElements()) {
					String header = headers.nextElement().toString();
					System.out.println(header + ": " + req.getHeader(header));
				}
				Date now = new Date();
				System.out.println(now.toString() + " Created PlayoutContext for recording " + recording + ": " + req.getRemoteHost() + "/" + req.getHeader("x-forwarded-for") + " (" + req.getHeader("User-Agent") + ")");
			} else {
				String id = req.getParameter("id");
				if(id == null || id.equals("")) {
					sendErrorResponse(resp, "Missing id");
					return;
				}
				pc = contexts.get(id);
				if(pc == null) {
					sendErrorResponse(resp, "No playout context");
					return;
				}
			}
			long start = System.currentTimeMillis();
			boolean empty = false;
			pc.resetTimeout();
			while(System.currentTimeMillis() - start < 30000) {	// Polls last 30 seconds at max
				synchronized(pc.messages) {
					empty = pc.messages.isEmpty();
				}
				if(empty) {
					try {
						Thread.sleep(100);	// Wait 100ms before checking again
					} catch (InterruptedException e) {
						e.printStackTrace();
						break;
					}
					continue;
				}
				break;
			}
			resp.setContentType("application/xml");
			resp.setCharacterEncoding("UTF-8");
			synchronized(pc.messages) {
				if(pc.messages.isEmpty()) {
					System.out.println("No messages in the queue and " + (System.currentTimeMillis()-start) + "ms passed");
					resp.getWriter().write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
					resp.getWriter().write("<meetechoweb/>");	// Nothing happened
				} else {
					System.out.println("Got an event (" + pc.messages.size() + "), and only needed to wait " + (System.currentTimeMillis()-start) + "ms");
					resp.getWriter().write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
					resp.getWriter().write("<meetechoweb>");
					while(!pc.messages.isEmpty()) {
						resp.getWriter().write(pc.messages.remove());	// First message in the queue (head)					
					}
					resp.getWriter().write("</meetechoweb>");
				}
			}
			return;
		} else if(action.equals("play")) {
			System.out.println("REQUEST PLAY");
			String id = req.getParameter("id");
			if(id == null || id.equals("")) {
				sendErrorResponse(resp, "Missing id");
				return;
			}
			PlayoutContext pc = contexts.get(id);
			if(pc == null) {
				sendErrorResponse(resp, "No playout context");
				return;
			}
			pc.resetTimeout();
			pc.play();
		} else if(action.equals("pause")) {
			System.out.println("REQUEST PAUSE");
			String id = req.getParameter("id");
			if(id == null || id.equals("")) {
				sendErrorResponse(resp, "Missing id");
				return;
			}
			PlayoutContext pc = contexts.get(id);
			if(pc == null) {
				sendErrorResponse(resp, "No playout context");
				return;
			}
			pc.resetTimeout();
			pc.pause();
		} else if(action.equals("seek")) {
			String id = req.getParameter("id");
			if(id == null || id.equals("")) {
				sendErrorResponse(resp, "Missing id");
				return;
			}
			PlayoutContext pc = contexts.get(id);
			if(pc == null) {
				sendErrorResponse(resp, "No playout context");
				return;
			}
			String ms = req.getParameter("ms");
			if(ms == null || ms.equals("")) {
				sendErrorResponse(resp, "Missing ms");
				return;
			}
			try  {
				long time = Long.parseLong(ms);
				pc.resetTimeout();
				pc.seek(time);
			} catch(NumberFormatException e) {
				sendErrorResponse(resp, "Invalid ms");
				return;
			}
		} else if(action.equals("reset")) {
			String id = req.getParameter("id");
			if(id == null || id.equals("")) {
				sendErrorResponse(resp, "Missing id");
				return;
			}
			PlayoutContext pc = contexts.get(id);
			if(pc == null) {
				sendErrorResponse(resp, "No playout context");
				return;
			}
			pc.resetTimeout();
			pc.reset();
		} else if(action.equals("restart")) {
			String id = req.getParameter("id");
			if(id == null || id.equals("")) {
				sendErrorResponse(resp, "Missing id");
				return;
			}
			PlayoutContext pc = contexts.get(id);
			if(pc == null) {
				sendErrorResponse(resp, "No playout context");
				return;
			}
			pc.resetTimeout();
			pc.reset();
		}
	}

	@Override
	public void run() {
		System.out.println(" *** Starting thread");
		long before = System.currentTimeMillis();
		long now = before;
		Vector<String> cleanup = new Vector<String>();
		while(contexts != null) {
			now = System.currentTimeMillis();
			if(contexts == null)
				break;
			synchronized(contexts) {
				if(!contexts.isEmpty()) {
					//					System.out.println(" ## Checking users for inactivity");
					Iterator<String> iter = contexts.keySet().iterator();
					while(iter.hasNext()) {
						String id = iter.next();
						if(id == null)
							continue;
						PlayoutContext pc = contexts.get(id);
						if(pc == null)
							continue;
						if(now-pc.getTimeout() > 60*1000) {
							System.out.println(" ## ## PlayoutContext (" + pc.getId() + ") has been inactive for " + (now-pc.getTimeout()) + "ms!");
							cleanup.add(id);
							//						} else {
							//							System.out.println(" ## ## PlayoutContext (" + pc.getId() + ") is ok");
						}
					}
					if(!cleanup.isEmpty()) {
						iter = cleanup.iterator();
						while(iter.hasNext()) {
							String id = iter.next();
							System.out.println(" ## ## Removing PlayoutContext (" + id + ") for inactivity");
							PlayoutContext pc = contexts.get(id);
							pc.stop();
							contexts.remove(id);
							pc = null;
						}
						cleanup.clear();
					}
				}
			}
			try {
				before = now;
				Thread.sleep(5000);	// Wait 5 seconds before checking again
			} catch (InterruptedException e) {
				e.printStackTrace();
				break;
			}
		}
		System.out.println(" *** Leaving thread");
	}

}
