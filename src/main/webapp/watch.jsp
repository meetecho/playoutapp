<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.meetecho.ajax.recordings.smil.SmilManager"%>
<%@ page import="com.meetecho.ajax.recordings.smil.SmilDescription"%>
<%
	response.setHeader("Pragma", "no-cache");
	response.setHeader("Cache-control",
			"no-cache, no-store, must-revalidate");
	response.setHeader("Expires", "0");
	request.getSession().removeAttribute("context");
	SmilManager sm = SmilManager.getInstance();
	if (sm == null) {
		response.sendError(HttpServletResponse.SC_SERVICE_UNAVAILABLE);
		return;
	}
	String redirect = sm.getRedirectUrl();
	String title = null, name = null;
	String recording = request.getParameter("recording");
	String policy = request.getParameter("p");
	Integer sSec = (request.getParameter("t")==null) ? 0 : Integer.parseInt(request.getParameter("t"));
	
	if (recording == null || recording.equals("")) {
		request.getSession().removeAttribute("smil");
		response.sendError(HttpServletResponse.SC_NOT_FOUND);
		return;
	}

	if (policy==null || policy.equalsIgnoreCase(""))
		policy = "0";//LOSSY IS DEFAULT 

	if (sSec==null || sSec<0)
		sSec = 0; //START FROM BEGIN
	
	String chapter = request.getParameter("chapter");
	String autoplay = request.getParameter("autoplay");
	SmilDescription sd = null;
	if (chapter == null) {
		if (!sm.recordingExists(recording)) {
			request.getSession().removeAttribute("smil");
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
			return;
		}
		sd = sm.getSmilDescription(recording);
		title = recording;
		name = recording;
	} else {
		if (!sm.recordingExists(recording, chapter)) {
			request.getSession().removeAttribute("smil");
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
			return;
		}
		sd = sm.getSmilDescription(recording, chapter);
		title = recording + " (" + chapter + ")";
		name = recording + "-" + chapter;
	}
	request.getSession().setAttribute("smil", sd);
	String userAgent = request.getHeader("User-Agent");
	System.out.println("User-Agent: " + userAgent);
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="expires" content="0">
<script type="text/javascript" src="jss/jquery-1.9.1.js"></script>
<script type="text/javascript" src="jss/modernizr-1.7.min.js"></script>
<script type="text/javascript" src="jss/Meetecho.RecCORE.js?10"></script>
<script type="text/javascript" src="jss/Meetecho.RecCHAT.js?10"></script>
<script type="text/javascript" src="jss/Meetecho.RecGUI.js?10"></script>
<script type="text/javascript" src="jss/Meetecho.RecWB.js"></script>
<script type="text/javascript" src="jss/bootstrap.min.js"></script>
<script type="text/javascript" src="jss/jquery-ui-1.10.3.custom.min.js"></script>
<script type="text/javascript"
	src="jss/jquery.i18n.properties-min-1.0.9.js"></script>
<script type="text/javascript" src="html5wb/extend_canvas.js"></script>
<script type="text/javascript" src="html5wb/trim-canvas.js"></script>
<script type="text/javascript" src="html5wb/wb_library.js"></script>
<script type="text/javascript" src="html5wb/wb_utils.js"></script>
<script type="text/javascript">
 jQuery.i18n.properties({
	    name:'recording', 
	    path:'i18n/', 
	    language: 'en_EN', 
	    callback: function() {
	    	console.log("Language loaded!");
	    }
	});
 
 	//Audio support var 	<%=sd.getAudioFile()%>';
	var webASource = '<%=sd.getAudioFile()%>'; 
	var recStartTime = <%=sd.getRecStartTime()%>;
	var recEndTime = <%=sd.getRecEndTime()%>;
	recording = '<%=name%>';
	var loadPolicy = <%=policy%>;
	var startSec = <%=sSec%>;
</script>

<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>

<link rel="icon" href="favicon.ico" />
<link rel="stylesheet" type="text/css"
	href="css/jquery-ui-1.10.3.custom.min.css" />
<link rel="stylesheet" type="text/css"
	href="css/Meetecho.Whiteboard.css" />
<link rel="stylesheet" type="text/css"
	href="css/bootstrap-responsive.css" />
<link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="html5wb/docebo/common.css">
<link rel="stylesheet" type="text/css" href="css/scroll.css">
<link rel="stylesheet" type="text/css" href="css/Meetecho.Recording.css" />
<title>Watch Recording: <%=title%></title>
</head>
<body onload="setTimeout('startEvents()', 500);">


	<!-- Modal -->
	<div id="meetcho-modal-container">
		<div id="meetcho-modal" class="modal hide fade" tabindex="-1"
			role="dialog" aria-labelledby="ModalCom" aria-hidden="true"></div>
	</div>

	<header>
		<div class="header row-fluid">
			<div class="span2" id="logoimg" style='margin-top: -1px'><img src="img/Loghi/Meetecho.png"></div>
			<div class="title span4" id="info">
				<div id="rectitle">
					<strong><%=title%></strong>
				</div>
			</div>
			<div class="navbar span6">
				<div class="navbar-inner">
					<div class="pull-right rightpanel">
						<ul class="nav">
							<li class="tooltip-meet">
								<div></div>
							</li>
							<li class="panel-btn" id="tab-clear-li">
								<div class="clear first">
									<div id="tab-clear" data-modal="modal_error.html" href="#clear"
										original-title="Show slides" data-panel="#clear">
										<img src="img/icon/close_gray.png">
									</div>
								</div>
							</li>
							<li class="panel-btn" id="tab-presentations-li">
								<div class="presentation">
									<div id="tab-presentations" data-modal="modal_load_slide.html"
										href="#presentations" original-title="Show slides"
										data-panel="#presentations">
										<img src="img/icon/pdf_gray.png">
									</div>
								</div>
								<div class="options-scrolldown slide">
									<div class="option" id="stop" data-tooltip="Stop Presentation">
										<img src="img/icon/close_gray.png">
									</div>
									<div class="option" id="load" data-tooltip="Change Slide Block">
										<img src="img/icon/inboxout_gray.png">
									</div>
								</div>
							</li>
							<li class="panel-btn" id="tab-desktop-li">
								<div class="screensharing">
									<div id="tab-r5desktop" href="#r5desktop"
										original-title="Show desktop sharing (Flash)"
										data-panel="#r5desktop">
										<img src="img/icon/display_gray.png">
									</div>
								</div>
							</li>
							<li class="panel-btn" id="tab-wb-li">
								<div class="whiteboard last">
									<div id="tab-whiteboard" href="#whiteboard"
										original-title="Show Whiteboard" data-panel="#whiteboard">
										<img src="img/icon/brush_gray.png">
									</div>
								</div>
							</li>
							<!--
							<li class="panel-btn" id="tab-audio-li">
								<div class="audiovideo last">
									<div id="tab-audio" href="#audio"
										original-title="Show audio/video options" data-panel="#audio">
										<img src="img/icon/audiovideo_gray.png">
									</div>
								</div>
							</li>
							-->
						</ul>
					</div>
				</div>
			</div>
		</div>
	</header>

	<div class="container-fluid maincontent">
		<div class="row-fluid">
			<div class="span3 leftbar" style="display: none">
				<!-- LEFTBAR/CHAT -->
				<div class="video" id="videoTeacher"></div>
				<!--  ACCORDION MENU -->
				<div class="accordion" id="meetecho_accordion">
					<div class="accordion-group" id="publicchat">
						<div class="accordion-heading">
							<a class="accordion-toggle active" data-toggle="collapse"
								data-parent="#meetecho_accordion" href="#collapseTwo"> <img
								class="icon" src="img/icon/chat_white.png" /> <span></span>
								<div class="pull-right">
									<span class="badge badge-info not-displayed">0</span>
									<div class="accordion-arrow">
										<img src="img/icon/uparrow_white.png">
									</div>
								</div>
							</a>
						</div>
						<div id="collapseTwo" class="accordion-body collapse in">
							<div class="accordion-inner leftbar-chat">
								<div class="box chat-normal" id="chatbox">
									<div class="box-inner" id="chat" tabindex="5012"></div>
								</div>
							</div>
						</div>
					</div>
					<div class="accordion-group" id="tab-polling">
						<div class="accordion-heading">
							<a class="accordion-toggle" data-toggle="collapse"
								data-parent="#meetecho_accordion" href="#collapseThree"> <img
								class="icon" src="img/icon/ok_blue.png" /> <span></span>
								<div class="pull-right">
									<div class="accordion-arrow" class="accordion-arrow">
										<img src="img/icon/downarrow_black.png">
									</div>
								</div>
							</a>
						</div>
						<div id="collapseThree" class="accordion-body collapse">
							<div class="accordion-inner">
								<div class="poll" id="pollopts"></div>
							</div>
						</div>
					</div>
				</div>
				<!-- END ACCORDION  -->
				<div id='hidden_modal'></div>
			</div>
			<!-- END LEFTBAR/CHAT -->
			<div class="wrapper span7">
				<!-- WRAPPER -->

				<!-- Start Presentation Area -->
				<div id="presentations" class="meetecho-feature slideview">
					<img class="slideimg" id="slideimg" src="img/logo.png" />
				</div>
				<div id="whiteboard" class="meetecho-feature whiteboard"></div>
				<div id="conference_mode" class="meetecho-feature conference row-fluid"></div>
				<!-- End Presentation Area -->
				<!-- Subtitles -->
				<div id="overlaySub">
					<div id="subMsg"></div>
				</div>
			</div>
			<!-- END WRAPPER -->
			
			<div class="video span2" id="videoStudent"></div>
		</div>
	</div>
	<!-- END MAINCONTENT -->

	<!-- Overlay Boxes -->
	<div id="overlay">
		<div id="overlayMsg"></div>
	</div>
	<footer>
		<div id="extra">
			<div id="controls" class="span2">
                <button id="subbtn" class>Sub</button>
				<button id="playpause" class="disabled" onclick="playControl()"
					style="background-image: url(img/icon/rec_play.png);">
				</button>
				<button id="resetbtn" onclick="resetControl()"
                	style="background-image: url(img/icon/rec_stop.png);">
	            </button>
				
				<span id="time">00:00:00/00:00:00</span>
			</div>
			<div id="seeker" class="span10">
				<div id="seek"
					class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all ui-slider-disabled ui-state-disabled"
					aria-disabled="true">
					<a class="ui-slider-handle ui-state-default ui-corner-all" href="#"
						style="left: 2%;"></a>
				</div>
			</div>
			<span class="span2 pull-right logomeetecho"> <img src="img/Loghi/Meetecho.png">
			</span>
		</div>
	</footer>

</body>
</html>