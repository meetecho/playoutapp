var audio, recording, id, t, seekTime = 0, playing, buttonPause, subTimeout;
var timeLabelStore = new Object();

function playControl() {
	if (!$('#playpause').hasClass("disabled"))
		if (buttonPause == false) {
			$('#playpause').addClass('disabled');
			$('#seek').slider('disable');
			sendRequest("action=pause");
		} else {
			$('#playpause').addClass('disabled');
			sendRequest("action=play");
		}
}

function startCount() {
    console.warn("OKOK")
	window.clearInterval(t);
	t = window.setInterval(function() {
		if(seekTime < recEndTime) {
			if(isNaN(recEndTime))
				return;
			seekTime++;
			sendRequest('action=seek&ms=' + Math.floor(seekTime*1000));
			var seconds = Math.floor(seekTime);
			var minutes = Math.floor(seconds/60);
			seconds = seconds-(minutes*60);
			if(seconds<10)
				seconds = '0' + seconds;
			var hours = Math.floor(minutes/60);
			minutes = minutes-(hours*60);
			if(minutes<10)
				minutes	= '0' +	minutes;
			if(hours<10)
				hours = '0' + hours;	
			var cur = '' + hours + ':' + minutes + ':' + seconds;
			seconds = Math.floor(recEndTime);
			minutes = Math.floor(seconds/60);
			seconds = seconds-(minutes*60);
			if(seconds<10)
				seconds	= '0' +	seconds;
			hours = Math.floor(minutes/60);
			minutes = minutes-(hours*60);
			if(minutes<10)
				minutes = '0' + minutes;
			if(hours<10)
				hours = '0' + hours;
			var tot = '' + hours + ':' + minutes + ':' + seconds;
			$('#time').html(cur + "/" + tot);
			var seek = Math.floor(seekTime*100/recEndTime);
			$('#seek').slider('value', seek);
		} else {
			sendRequest('action=reset');
		}
	},1000);
}

function pauseCount() {
	window.clearInterval(t);
}

function getRandomId() {
	var rId = "rid=" + new Date().getTime() + "&";
	return rId;
}

function startEvents() {
	// Start polling for events: as soon as a reply arrives, we send a new one
	getUpdates(true);
}

function getUpdates(first) {
	var data = getRandomId() + "action=getUpdate";
	if(first === true) {
		data = data + "&first=true&recording=" + recording;	// Restart recording, if needed
	} else {
		data = data + "&id=" + id;
	}
	$.ajax({
		type: 'POST',
		url: "./Controller",
		cache: false,
		contentType: "application/x-www-form-urlencoded;charset=UTF-8",
		data: data,
		success: function(xml) {
			$(xml).find("meetechoweb").each(function(k) {

				// Alert
				$(this).find("alert").each(function(i) {
				    var titleAlert = $(this).find("title").text();
                    var contentAlert = $(this).find("content").text();

				    if (titleAlert.toLowerCase()=="playout stopped")
				        contentAlert = rec_ended;

   					errorMessage(titleAlert, contentAlert);
				});

				// Status
				$(this).find("status").each(function(i) {
					var status = $(this).text();

					if(status === 'setup') {
						id = $(this).attr('id');
						$('#playpause').removeClass('disabled').css('background-image','url("img/icon/rec_play.png")');
						buttonPause = true;
					} else if(status === 'paused') {
						$('#playpause').removeClass('disabled').css('background-image','url("img/icon/rec_play.png")');		
						buttonPause = true;

						$.event.trigger({
							type: "MeetechoPause"
						});
					} else if(status === 'configured') {
						$.event.trigger({
							type: "MeetechoConfigured",
						});
					} else if(status === 'playing') {
						buttonPause = false;
						$('#seek').slider('enable');
						$('#playpause').removeClass('disabled').css('background-image','url("img/icon/rec_pause.png")');
						if(audio.currentSrc != null) { //FIXME
//							if(audio.currentSrc.indexOf('.wav') != -1) {
//								$('#vtitle').html('<a href="' + audio.currentSrc + '">Download audio (WAV)</a>');
//							} else if(audio.currentSrc.indexOf('.mp4') != -1) {
//								$('#vtitle').html('<a href="' + audio.currentSrc + '">Download audio (MP4)</a>');
//							} else if(audio.currentSrc.indexOf('.wav16') != -1) {
//								$('#vtitle').html('<a href="' + audio.currentSrc + '">Download audio (WAV16)</a>');								
//							} else if(audio.currentSrc.indexOf('.mp3') != -1) {
//								$('#vtitle').html('<a href="' + audio.currentSrc + '">Download audio (MP3)</a>');								
//							} else {
//								errorMessage(no_codec_title, no_codec_text, function () {javascript:history.back();});
//							}
						} else {
							errorMessage(no_codec_title, no_codec_text, function () {location.href='http://ietf91.conf.meetecho.com/';});
						}
						$.event.trigger({
							type: "MeetechoPlay",
							origin: "user"
						});
					} else if(status === 'reset') {
						$.event.trigger({
							type: "MeetechoReset",
						});
					}						
				});
				// Slide
				$(this).find("SLIDE").each(function(i) {
					console.log("SLIDE - "+$(this).text());
					
					if (!$(".video-slide-content").length) {
						var url = $(this).text();

						if (url.indexOf("stop.jpg")!=-1) {
							$("#slideimg").fadeOut("fast");
							url = "img/stop.jpg";
							return;
						} else if (url.indexOf("uploading.png.jpg")!=-1)
							url = "img/uploading.jpg";

						$("#slideimg").fadeOut('fast', function() {
							$(this).attr("src", url);
						});
						$("#slideimg").fadeIn("slow");
						tabNotifier("presentations");
					}
				});

				// Chat
				$(this).find("jabber").each(function(i) {
					if($("#" + $(this).attr('id')).length)
						return;
					var msg = chat_string_create_urls($(this).text());

					if(msg.indexOf('] ') != -1) {
						var start = msg.indexOf('] ') + 2;
						var currentTime = msg.substring(0, start);
						var content = msg.substring(start);

						if(content.indexOf('***') == 0) {
							// Some join or status message
							addChatMsg("***", content, $(this).attr('id'), currentTime);
						} else if(msg.indexOf(': ') != -1) {
							// A text message
							var end = msg.indexOf(': ') + 2;
							var username = msg.substring(start, end);
							var textMsg = msg.substring(end);
							addChatMsg(username, textMsg, $(this).attr('id'), currentTime);

						} else {
							// Whatever
							var whatever = msg.substring(start);
							addChatMsg("***", whatever, $(this).attr('id'), currentTime);
						}

					} else 
						addChatMsg("<<<", msg, $(this).attr('id'));
				});
				// Remove
				$(this).find("remove").each(function(i) {
					var del = $(this).text();
					console.log("remove "+del);
					if($("#" + del).length) {
						while($("#" + del).length) {
							$("#" + del).remove();
						}
//						$("#" + del).nextAll('.chat-msg-system').remove();
					}
					if (del.indexOf("SUB")!=-1) {
						console.log("removing SUB");
						$("#subMsg").text("");
						if (!typeof subTimeout == 'undefined')
							subTimeout.reset();
					}
					if (del.indexOf("VIDEO")!=-1)
						nextTalker();
					if (del.indexOf("POLL")!=-1)
						$("#pollopts").empty();
					if (del.indexOf("WHITEBOARD")!=-1) {
						wbID = $(this).attr("wbID");
						shapeID = $(this).attr("shapeID");
						if (shapeID!=null) {
							console.log("removig from wb "+wbID+" : "+shapeID);
							if(conference !== undefined && conference !== null) {
								conference.getWBbyID(wbID).removeShapeByID(shapeID);
							}							
						} else {
							console.log("closing tab");
							conference.closeTab();
						}

					}
					element_resize();
				});

				$(this).find("POLL").each(function(i) {
					$(this).find("results").each(function(j) {
						$tempresult = $(this);
						showPollResult($tempresult);
						tabNotifier("poll");
					});

				});

				$(this).find("PRELOAD").each(function(i) {
					if (loadPolicy==2 || loadPolicy==3) {
						var id = $(this).attr("id");
						if (!$("#"+id).length) { 
							var path2video = $(this).text();
							var durate = $(this).attr("start");
							addVideo(path2video, id, durate, false);
						}
					} 
				});

				$(this).find("VIDEO").each(function(i) {
					var id = $(this).attr("id");
					var start = $(this).attr("start");
					var end = $(this).attr("end");
					if (loadPolicy==0 || loadPolicy==1){
						if (!$("#"+id).length) {
							var path2video = $(this).text();
							addVideo(path2video, id, start, end, false);  //was false
						}
					} else
						initControl(streamsMapFlat[id][0], durate);
				});

				$(this).find("VIDEOTC").each(function(i) {
					var id = $(this).attr("id");
					var start = $(this).attr("start");
					var end = $(this).attr("end");
					if (loadPolicy==0 || loadPolicy==1) {
						if (!$("#"+id).length) { 
							var path2video = $(this).text();
							addVideo(path2video, id, start, end, false);  //was false
						}
					} else
						initControl(streamsMapFlat[id][0], durate);
				});

				$(this).find("LABEL").each(function(i) {
					var id = $(this).attr("id");
					var effectiveTime = $(this).attr("start");
					var label = $(this).text();
					$("#seeker").prepend("<div class='etiquette' id='"+id+"'></div>");
					$("#"+id).css("left",Math.floor(effectiveTime*100/recEndTime)+"%");
					$("#"+id).tooltip({
						title: label
					});
					$("#"+id).on("click", function () {
						console.log("To mark at " +effectiveTime);
						seekTime = effectiveTime;
						seekVideos(seekTime);
					});				
					timeLabelStore[label] = effectiveTime;
				});
				
				$(this).find("VIDEOSLIDE").each(function(i) {
					var id = $(this).attr("id");
					var start = $(this).attr("start");
					var end = $(this).attr("end");
					if (loadPolicy==0 || loadPolicy==1) {
						if (!$("#"+id).length) { 
							var path2video = $(this).text();
							addVideoSlide(path2video, id, start, end, false);
						} 
					} else
						initControl(streamsMapFlat[id][0], durate);
				});
				
                $(this).find("SUB").each(function(i) {
                	var subText = $(this).text();
                    var startime = parseInt($(this).attr("start"));
                    startime += parseInt($(this).attr("durate")); 
                    var id = $(this).attr("id");
                    startime -= seekTime;
                    if (!typeof subTimeout == 'undefined')
                            subTimeout.reset(); //clearInterval(subTimeout);

                    $("#subMsg").text(subText);
                    $("#overlaySub").data("active",id);
                    if (subActive)
                            $("#overlaySub").show();
                    subTimeout = new Timer(function () { //setTimeout(function () {
                    	if ($("#overlaySub").data("active")==id) {
                    		$("#overlaySub").data("active","").hide();
                            $("#subMsg").text("");
                    	}
                    }, startime*1000);
                });

				
				$(this).find("WHITEBOARD").each(function(i) {
					tabNotifier("wb");
					init = $(this).attr("init");
					if (typeof init !== 'undefined' && init !== false) {

						if (conference.getWBbyID(init)==null) {
							console.log("creating WB with id "+init);
							addTab(parseInt(init)-parseInt(1));
							conference.wb_count++;
						}
//						element_resize("wb");
						if (conference.getWBbyID(init)!=null) 
							conference.getWBbyID(init).setBG(b64parse($(this).text()),"png","system");

					} else if(conference !== undefined && conference !== null) {
						conference.receiveIQ($(this));
					}
					//addChatMsg("<<<", paint_wb($(this).find("author").text()));
				});

			});
			// TODO
			setTimeout('getUpdates(false)', 200);
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			// TODO
		},
		dataType: "xml"
	});
}

function sendRequest(data) {
	$.ajax({
		type: 'POST',
		url: "./Controller",
		cache: false,
		contentType: "application/x-www-form-urlencoded;charset=UTF-8",
		data: getRandomId() + "id=" + id + "&" + data,
		success: function(xml) {
			$(xml).find("meetechoweb").each(function(k) {
				// Error?
				$(this).find("error").each(function(i) {
					errorMessage(request_error, $(this).text(), function () {window.location.reload();});
				});
			});
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			errorMessage(request_error, textStatus+" "+errorThrown);
		},
		dataType: "xml"
	});
}

function showPollResult($tempresult) {
	$("#pollopts").load("template_docebo/poll_resources.html #teacherpoll_ex", function() {
		$("#pollquestion").text($tempresult.find("question").text());
		$("#polltitle").text(vote_result);
		$("#pollparticipants > span:eq(0)").text(parseInt($tempresult.find("tot").text()));
		$("#pollparticipants > span:eq(1)").text(vote_active_participants);
		$("#pollparticipants > span:eq(2)").text(parseInt($tempresult.find("num").text()));
		$("#pollparticipants > span:eq(3)").text(vote_total_participants);
		$("#pollresults").text(poll_result_title);
		poll_color("reset");
		var datapoll = new Array();
		var dataname = new Array();
		$tempresult.find("option").each(function(k) {
			thiscolor = poll_color();
			dataname[k] = $(this).text();
			datapoll[k] = {
					name: $(this).text(),
					color: thiscolor,
					y: parseInt($(this).attr("votes"))
			};
		});
		pollchart_factory(dataname, datapoll);
		$("tspan:contains('Highcharts.com')").remove();
	});		
}

function pollchart_factory(XAxisTitle, series) {

	$("#pollgraph_ex").height(series.length*50);

	var chart = new Highcharts.Chart({

		chart: {
			renderTo: 'pollgraph_ex',
			type: 'bar', 
		},
		title: {
			text: null
		},
		legend: {
			enabled: false
		},
		xAxis: {
			categories: XAxisTitle,
			title: {
				enabled : false
			},
			min: 0,
			lineWidth: 0,
			minorGridLineWidth: 0,
			gridLineWidth: 0,
			lineColor: 'transparent',
			labels: {
				enabled: false
			},
			minorTickLength: 0,
			tickLength: 0,
		},
		yAxis: {
			title: {
				text: '',
				enabled : false
			},
			min: 0,
			gridLineWidth: 0,
			lineWidth: 0,
			minorGridLineWidth: 0,
			lineColor: 'transparent',
			labels: {
				enabled: false
			},
			minorTickLength: 0,
			tickLength: 0,
			stackLabels: {
				style: {
					color: 'white'
				},
				enabled: true,
				verticalAlign: 'middle',
				align: 'left'
			}
		},

		plotOptions: {
			bar: {
				shadow: false,
				pointPadding: 0.0
			}   
		},

		series: [{
			data: series,
			dataLabels: {
				enabled: true,
				color: '#FFFFFF',
				align: 'right',
				formatter: function() {
					return this.y;
				},
				style: {
					fontSize: '10px',
					fontFamily: '"Helvetica Neue", Helvetica, Arial, sans-serif'
				}
			}
		}],

		tooltip: {
			formatter: function () {
				return this.x;
			},
			shadow: false
		}
	});
}


function seekVideos(seekTime) {
	$("body").find("video").each(function () {
		thisVideoStartTime = streamsMapFlat[$(this).attr("id")][1];
		if (parseInt(thisVideoStartTime)<=parseInt(seekTime))		
			streamsMapFlat[$(this).attr("id")][0].get(0).currentTime = parseInt(seekTime)-parseInt(thisVideoStartTime);
		else {
			$(this).remove();
			element_resize();
			removeStreams($(this).attr("id"));
		}
	});
}

function videosPause($this) {
	$this.pause();
}

function videosPlay($this) {
	$this.play();
}

function videosReset($this) {
	$this.currenTime = 0;
}

function resetControl() {
	$.event.trigger({
		type: "MeetechoReset",
	});
}

function needReseek($element, sTime, start) {

	console.log("RESEEK " + $element.attr("id") + " sTime, start : "+sTime+", "+start);

    if ($element.stat=='block' || $element.stat=='buffering')
        return false;

    var cTime = $element.get(0).currentTime;
	var control = Math.abs(cTime - (parseInt(seekTime)-parseInt(start)));
	console.log("need reseek? "+control+" ("+cTime+","+sTime+","+start+") ");


	return (control>1);
}

function initControl($element, sMark) {
	console.log("SHOWING VIDEO "+$element.attr("id"));
	$element.show();
	if (playing)
		$element.get(0).play();
	$element.on('canplay', function () {
		if (needReseek($element.get(0).currentTime, parseInt(seekTime), parseInt(sMark)))
			$element.get(0).currentTime = parseInt(seekTime)-parseInt(sMark);
	});
	disposeControls($element, sMark, false);
}

//SUBS TIMER
function Timer(callback, delay) {
    var timerId, start, remaining = delay;
    
    this.pause = function() {
        window.clearTimeout(timerId);
        remaining -= seekTime*1000 - start;
    };

    this.resume = function() {
    	if (typeof start != 'undefined')  //just update!!
    		remaining += (parseInt(start) - seekTime*1000);
        start = seekTime*1000;//new Date();
        timerId = window.setTimeout(callback, remaining);
    };

    this.reset = function() {
        window.clearTimeout(timerId);
        remaining = 0;
        delete this;
    };

    this.resume();
}


