        var rate_43 = false;
        var subActive = true;
        var $video_conference;
        var $video_container;
        var recording;
        var active_service = 3;
        var wait_msg = 0;
        var forceMode = false;
        var screenShowed = "presentations";
        var switching = false;
        var streamsMapFlat = new Object();
        var availableStreams = 1;
        var isReset = false;
        var waitingStreams = 0;
        var theme = "Standard";
        var canvasLoop;
        var bufferCheck = new Array();

        //Active features
        var hasWB = false, hasPOLL = false;

        /***
         * LOAD POLICIES:
         * 0:	Lossy 		- Only videoteacher and audio stops play when buffering
         * 1:   HiQ			- All videos stops play when buffering
         * 2: 	LPreload	- Videos are loaded 5 sec before play, then Lossy
         * 3: 	HiQPreload	- Videos are loaded 5 sec before play, then HiQ
         */

        function getStreamsFlat(k) {
            return streamsMapFlat[k];
        }

        function removeStreams(k) {
            delete streamsMapFlat[k];
        }


        $(document).ready(function() {

                $("body").hide();
                setTimeout(function () {
                    $("body").fadeIn();
                }, 750);

                setTimeout(element_resize, 5000);

            //Loading Theme
            var cssLink = $("<link rel='stylesheet' type='text/css' href='css/Meetecho."+theme+".css'>");
            $("head").append(cssLink);

            element_resize();
            $(window).resize(function () {
                element_resize();
            });

            //Subs Controller *EXP*
            $("#subbtn").on("click", function () {
                if (!subActive) {
                    $("#subbtn").attr("class", ""); //css("background","rgb(211, 144, 3)");
                    $("#overlaySub").show();
                    subActive = true;
                } else {
                    $("#subbtn").attr("class", "off");//.css("background","rgb(120, 120, 120)");
                    $("#overlaySub").hide();
                    subActive = false;
                }
            });

            //Video Helper
            $(document).on('MeetechoPause', function () {
                if (!buttonPause)
                    if (!$("#overlay").is(":visible")) {
                        $("#overlayMsg").text("buffering...");
                        $("#overlay").fadeIn();
                    }

                console.warn("meetecho pause event fired");

                $("body").find("video").each(function (i) {
                    console.log($(this).attr("id")+ " " + $(this).html());
                    $(this).get(0).pause();
                });

                playing = false;
                pauseCount();
                if (!typeof subTimeout == 'undefined')
                    subTimeout.pause();
            });

            $(document).on('MeetechoConfigured', function () {
                console.warn("meetecho configured event fired");
                setTimeout(function () {
                    element_resize("teacher");
                }, 1000);
                setTimeout(function () {
                    $("#videoStudent").fadeIn();
                }, 750);
                setUpChat();
                seekTime = startSec;
            });

            $(document).on('MeetechoPlay', function () {
                if (playing)
                    return;

                if ($("#overlay").is(":visible"))
                    $("#overlay").fadeOut();

                console.warn("meetecho play event fired");
                $('#seek').slider('enable');
                $("body").find("video").each(function (i) {
                    if (!$(this).is("#audio")) {
                        var sMark = streamsMapFlat[$(this).attr("id")][1];
                        $(this).get(0).play();
                        if (needReseek($(this), parseInt(seekTime), parseInt(sMark)))
                            $(this).get(0).currentTime = parseInt(seekTime)-parseInt(sMark);

                    } else if ($(this).is("#audio") && $(this).get(0).duration>seekTime) {
                        this.stat = "have2play";
                        $(this).get(0).play();
                        if ($(this).get(0).currentTime!=seekTime)
                            $(this).get(0).currentTime = seekTime;
                    }
                });
                playing = true;
                startCount();
                if (typeof subTimeout != 'undefined')
                    subTimeout.resume();
            });

            $(document).on('MeetechoReset', function () {
                console.warn("meetecho reset event fired");
                isReset = true;

                $('#playpause').css('background-image','url("img/icon/rec_play.png")');
                pauseCount();
                playing = false;
                buttonPause = true;

                //Slidebar
                $('#seek').slider('disable');
                seekTime = 0;

                $(".slideimg").hide();

                //clean Chat
                $("#chat").empty();

                //Videos
                $("body").find("video").each(function (i) {
                    if (parseInt(streamsMapFlat[$(this).attr("id")][1])>1) {
                        $("#"+$(this).attr("id")+"_canvas").remove();
                        $(this).remove();
                        element_resize();
                    } else {
                        $(this).get(0).currentTime = 0;
                        $(this).get(0).pause();
                    }
                });

                if (hasPOLL)	//POLL
                    $("#pollopts").empty();

                if (hasWB)	//Whiteboard
                    for (i=0; i<=conference.wb_count; i++)
                        conference.wb[i].closeTab();

                $('#seek').slider('value', 0);

            });

            $(document).on('VideoWaiting', function () {
                waitingStreams++;
//                console.warn("VideoWaiting event fired, there are " + waitingStreams + " waiting Streams");
                $.event.trigger({
                    type: "MeetechoPause",
                });
            });

            $(document).on('VideoPlaying', function () {
                waitingStreams--;
//                console.warn("VideoPlaying event fired, there are " + waitingStreams + " waiting Streams");

                if (waitingStreams<=0) {
                    waitingStreams = 0;
                    if (isReset) isReset = false;
                    else if (!buttonPause)
                        $.event.trigger({
                            type: "MeetechoPlay",
                        });
                }
            });

            (function () {
                var wdt = screen.width;
                var hgt = screen.height;
                var ratio = findratio(wdt, hgt);
                if (wdt/ratio==4 && hgt/ratio==3) {
                    rate_43 = true;
                }
            }());

            var audioElement = '<div class="audio" id="audioContainer" style="display: none;">'
                +'<video id="audio" preload class="audio-content">';

            if (webASource != null)
                audioElement += '<source src="'+webASource+'" type="audio/wav"/>';

            audioElement += '</audio></div>';

            //Show Video
            $(".leftbar").prepend(audioElement);
            streamsMapFlat["audio"] = new Array($("#audio"), 0);

            disposeControls(streamsMapFlat["audio"][0], 0, null, true, true);   //MOD HONOLULU1

            streamsMapFlat["audio"][0].on("ended", function () {
                console.log("Audio Ended!");

            });
            setTimeout(function () {
                $(".audio").fadeIn('slow');
            }, 750);


            if(!Modernizr.video) {
                // No HTML5 video support
                errorMessage(no_video_title, no_video_text, function() { location.href='http://ietf91.conf.meetecho.com/'; });
            }

            $video_conference = $("#conference_mode video").get(0);
            audio = $("#audioContainer video").get(0);

            $("#seek").slider({
                value: 1,
                min: 1,
                max: 100,
                step: 1,
                slide: function(event, ui) {
                    var tot = Math.floor(recEndTime);
                    var seek = tot*ui.value/100;
                    seekTime = seek;
                    seekVideos(seekTime);
                }
            });
            $('#seek').slider('disable');
            setTimeout(function() { startUpConfig(); },500);
        });


        function errorMessage(name, text, callback) {

                $('#meetcho-modal').modal('hide');
                $.get("template_docebo/modal_error.html", function(data) {
                    $('.modal.hide.fade').html(data);
                    //insert title
                    $('#modal-title').html(name);
                    //insert reason
                    $('#dialog-modal').html(text);

                    $('#meetcho-modal').unbind('hide').on('hide', function () {

                        $(".modal-backdrop").remove();
                        console.warn('modal hide called');
                        $('#meetcho-modal').empty();

                        if (callback!==undefined && typeof callback === 'function' )
                            callback();
                    });
                    $('#meetcho-modal').modal();
                });

        }


        function findratio(a,b) {
            return (b==0) ? a : findratio(b,a%b);
        }

        function element_resize(type) {
            type = (typeof type === "undefined") ? "all" : type;

            if (type=="all") {
                $("body").height($(window).height());
                $('.maincontent').height($(window).height() - ($("header").height() + 20 + $("footer").height()));
                $(".wrapper").height($('.maincontent').height() - 10);
                $('.tooltip-meet').width($('.tooltip-meet').closest(".rightpanel").width() - (active_service*48));
            }

            //leftbar
            if ((type=="all" || type=="leftbar") && $("#videoTeacher video").length<=0) {
                $('.leftbar').height($(".maincontent").height());
                $('#videoStudent').height($(".maincontent").height());
                $('#meetecho_accordion').css("height",$(".maincontent").height());
                $(".accordion-inner").height($(".maincontent").height()-82);
//                $(".accordion-inner").height(($(".maincontent").height()-41));
                $(".box.chat-normal").height(($(".maincontent").height()-101));
            } else if ((type=="all" || type=="teacher") && $("#videoTeacher video").length>0) {
                $('#meetecho_accordion').height(($(".maincontent").height()-$('#videoTeacher').height()+20));
                $(".accordion-inner").height((($(".maincontent").height()-$('#videoTeacher').height())-82));// ($(".accordion-heading").height()*2)-10);
                $(".box.chat-normal").height((($(".maincontent").height()-$('#videoTeacher').height())-101));
            }
            //slide
            if (type=="all" || type=="slide") {
        //		$(".slideimg").height($(".wrapper").height());
                $('.slideimg').width($(".wrapper").height()/3*4);
                $('.slidebuttons').width($(".slideimg").width());
                $('.slidetitle').width($(".slideimg").width());
                $('.slidetitle').css("top", 0 - $(".slideimg").height());
                $('.slidetitle_control').width($(".slideimg").width());
                $('.slideinfo').width($(".slideimg").width() - 90);

                //test slide redesign
                $('#presentations').find('canvas').each(function () {
                    var tmpId = $(this).attr('id');
                    console.warn(tmpId);
                    $(this).remove();
                    $('#presentations').append('<canvas id="'+tmpId+'" width="'+($(".wrapper").width())+'" height="'+($(".wrapper").width()*3/4)+'" ></canvas>');
                    if (canvasLoop!=null) {
                        clearInterval(canvasLoop);
                        var gotId = tmpId.split("_")[0];
                        var $this = $("#"+gotId).get(0); //cache
                        console.warn($this + " " + gotId);
                        var relCanvas = $("#"+tmpId).get(0);
                        (function newloop() {
                            try {
                                if (!$this.paused && !$this.ended) {
                                    relCanvas.getContext('2d').drawImage($this,0,0,relCanvas.width,relCanvas.height);
                                    canvasLoop = setTimeout(newloop, 1000 / 30); // drawing at 30fps
                                }
                            } catch (err) {
                                console.warn("WATCH!! 1 remove");
                                clearInterval(canvasLoop);
                                relCanvas.remove();
                            }
                        })();
                    }
                });
            }

            //whiteboard
            if (type=="wb") {
                console.log("Resizing WB");
                $("#wbtabs").height($(".wrapper").height());
        //		$(".buttonset").height(Math.floor($("#wbtabs").height()/12)).width(Math.floor($("#wbtabs").height()/12));
        //		$("#toolbar").height($("#wbtabs").height()).width($(".buttonset").height());
        //		$(".wb_more_options").height($(".buttonset").height()).width($(".buttonset").width());
        //		$(".wb-control").height($(".buttonset").height()).width($(".buttonset").height());
                $(".tab-content").height($("#wbtabs").height()-32);
                $(".canvasTab").height($(".wrapper").height()-$("#meetecho-wbtab.nav-tabs").height()-4).width($(".wrapper").width()-4);
            }

            if (type=="conference") {
                var count = 0;
                console.log("resize _conference called");
                $("#conference_mode").height($(".wrapper").height());

        //		console.log("There are "+x+"+1 active videos");

                if (!adaptiveVideo) {

                    if (rate_43) {
                        $("#conference_mode .videoavatar").not(".span6").addClass("span6").css("height","41.5%");
        //				$(".videoavatar:not(.hidden):nth-child(2n+1)").css("margin-left","0px");
                        $("#conference_mode .videoavatar").filter(
                                function(){
                                    console.log(!$(this).hasClass('hidden'));
                                    if (!$(this).hasClass('hidden')){
                                        count++;
                                        return count%2 == 1;
                                    }
                                }).css("margin-left","0px");
                    } else {
                        $("#conference_mode .videoavatar").not(".span4").addClass("span4");
                        $("#conference_mode .videoavatar").css("height","49%").css("margin-left",""); //reset margin for all
        //				$(".videoavatar:not(.hidden):nth-child(3n+1)").css("margin-left","0px");
                        $("#conference_mode .videoavatar").filter(
                                function(){
                                    if (!$(this).hasClass('hidden')){
                                        count++;
                                        return count%3 == 1;
                                    }
                                }).css("margin-left","0px");
                    }

                    $("#conference_mode #myVideo").css("float","left");
                    $("#conference_mode .videoavatar").css("width","");

                } else {
                    var x = (Object.keys(streamsMap).length); 				//FIXME remove only if Flash
                    $(".videoavatar").css("margin-left","");

                    if ((passiveMode || micMode) && x>0)
                        x--;

                    switch(true) {
                    case (x == 0): //Just Teacher
                        if (rate_43) $(".videoavatar").attr('class', 'videoavatar span12').css("height","128%");
                        else $(".videoavatar").attr('class', 'videoavatar span12').css("height","100%");
                    break;
                    case (x == 1): //Teacher and Student
                        if (rate_43) {
                            $(".videoavatar").attr('class', 'videoavatar span12').css("height","63%");
                            $(".videoavatar:eq(1)").css("margin-left","0px");
                        } else {
                            $(".videoavatar").attr('class', 'videoavatar span6').css("height","100%");
                            $("#myVideo").css("float","left");
                        }
                    break;

                    case (x == 2): //Teacher and 2 Student
                        if (rate_43) {
                            $(".videoavatar").attr('class', 'videoavatar span6').css("height","63%");
                            $("#myVideo").attr('class', 'videoavatar span12').css("float","left");
                            $(".videoavatar:eq(1)").css("margin-left","0px");
                        } else {
                            $(".videoavatar").attr('class', 'videoavatar span6').css("height","49%");
                            $(".videoavatar:eq(2)").css("margin-left","");
                            $("#myVideo").css("height","100%").css("float","left");
                        }
                    break;

                    case (x == 3): //Teacher and 3 Student
                        if (rate_43) {
                            $(".videoavatar").attr('class', 'videoavatar span6').css("height","63%");
                            $(".videoavatar:eq(1)").css("margin-left","");
                            $(".videoavatar:eq(2)").css("margin-left","0px");
                            $("#myVideo").css("float","left");
                        } else {
                            $(".videoavatar").attr('class', 'videoavatar span6').css("height","49%");
                            $(".videoavatar:eq(2)").css("margin-left","0px");
                            $(".videoavatar:eq(4)").css("margin-left","");
                            $("#myVideo").css("float","left");
                        }
                    break;

                    case (x == 4): //Teacher and 4 Student
                        if (rate_43) {
                            $(".videoavatar").attr('class', 'videoavatar span4').css("height","42%");
                            $(".videoavatar:eq(1),.videoavatar:eq(2)").css("margin-left","");
                            $(".videoavatar:eq(3)").css("margin-left","0px");
                            $("#myVideo").attr('class', 'videoavatar span8').css("height","84%").css("float","left");
                        } else {
                            $(".videoavatar").attr('class', 'videoavatar span4').css("height","49%");
                            $(".videoavatar:eq(2),.videoavatar:eq(4)").css("margin-left","");
                            $("#myVideo").css("height","100%").css("float","left");
                        }
                    break;

                    default:
                        if (rate_43) {
                            $(".videoavatar").attr('class', 'videoavatar span4').css("height","42%");
                            $(".videoavatar:eq(1),.videoavatar:eq(2)").css("margin-left","");
                            $(".videoavatar:eq(3)").css("margin-left","0px");
                            $("#myVideo").attr('class', 'videoavatar span8').css("height","84%").css("float","left");
                        } else {
                            $(".videoavatar").attr('class', 'videoavatar span4').css("height","49%");
                            $(".videoavatar:eq(4)").css("margin-left","0px");
                            $("#myVideo").css("float","left");
                        }
                    break;
                    }
                }
            }
        }

        function setLanguage() {

            console.log("Setting Language...");

            $("#publicchat").find(".accordion-toggle > span").text(chat_txt);
            $("#tab-polling").find(".accordion-toggle > span").text(poll_txt);

            $(".rightpanel .panel-btn#tab-clear-li").data("tooltip",clear_screen);
            $(".rightpanel .panel-btn#tab-presentations-li").data("tooltip",show_ppt);
            $(".rightpanel .panel-btn#tab-desktop-li").data("tooltip",show_ds);
            $(".rightpanel .panel-btn#tab-wb-li").data("tooltip",show_wb);
            $(".rightpanel .panel-btn#tab-audio-li").data("tooltip",show_conf);
            disposeTooltip(".rightpanel .panel-btn",".rightpanel .nav");

            disposeTooltip("#resetsearch","#resetsearch");

        }

        function disposeTooltip(launcher,activator) {
            //TOOLTIP
            $(launcher).unbind("hover").hover(function () {
                var help = $(this).data("tooltip");
                var tooltip = $(".tooltip-meet");
                $(tooltip).find("div").text(help);
            });

            $(activator).hover(
                    function () {
                        var tooltip = $(".tooltip-meet");
                        $(tooltip).find("div").stop(true).hide().fadeIn(250);
                    },
                    function () {
                        var tooltip = $(".tooltip-meet");
                        $(tooltip).find("div").fadeOut(250);
                        //setTimeout(function() {$(tooltip).find("div").text("");}, 100);
                    }
            );
        }

        function startUpConfig() {

            $('.panel-btn').hover(
                    function(e) {
                        $(this).find('img').each(function () {
                            var src_img = $(this).attr("src").split("_")[0];
                            $(this).attr("src", src_img+"_white.png");
                        });
                    },
                    function(e) {
                        if (!$(this).hasClass("active")) {
                            $(this).find('img').each(function () {
                                var src_img = $(this).attr("src").split("_")[0];
                                $(this).attr("src", src_img+"_gray.png");
                            });
                        }
                    });

            //CONTROLPANEL BUTTON CLICK
            $('.panel-btn').on('click', function() {
                if (!forceMode) {
                    if (!$(this).hasClass('active'))
                        $(this).addClass('active');

                    $(this).siblings(".active").toggleClass('active')
                    .find('img').each(function () {
                        var src_img = $(this).attr("src").split("_")[0];
                        $(this).attr("src", src_img+"_gray.png");
                    });
                }
                //FIXME ADD SWITCH TAB HANDLER
            });

            $("#tab-presentations-li").on('click', function () {
                if (forceMode)
                    return true;
                showSlide();
            });

            $("#tab-wb-li").on('click', function () {
                if (forceMode)
                    return true;
                showWb();
            });


            $('#meetecho_accordion').on('show hide', function(e) {

                $(e.target).siblings('.accordion-heading').find('.accordion-toggle .accordion-arrow > img').toggleAttr('src', "img/icon/uparrow_white.png", "img/icon/downarrow_black.png")
                $(e.target).siblings('.accordion-heading').find('.accordion-toggle').toggleClass('active');

                if ($(e.target).parent("#publicchat").length) {
                    $(this).find(".badge").addClass("not-displayed");
                    wait_msg = 0;
                }

                $(e.target).siblings('.accordion-heading').find('.accordion-toggle .icon').each(function () {
                    var src_img = $(this).attr("src").split("_")[0];
                    $(this).toggleAttr("src", src_img+"_white.png", src_img+"_blue.png");
                });


            });

            $('#meetecho_accordion').collapse({ parent: true, toggle: true });

            $('#playpause').css('background-image','url("img/icon/rec_wait.png")');

            setLanguage();
            setTimeout(function () {
                element_resize();
                $(".leftbar").fadeIn();
                $('#tab-whiteboard').closest(".panel-btn").fadeIn();
                $('#tab-presentations').closest(".panel-btn").fadeIn();
                $('#tab-audio').closest(".panel-btn").fadeIn();
                $('#presentations').fadeIn();
            }, 900);

            startWB();
        }


        //TOGGLE ATTR FN
        $.fn.toggleAttr = function(attr, attr1, attr2) {
            return this.each(function() {
                var self = $(this);
                if (self.attr(attr) == attr1)
                    self.attr(attr, attr2);
                else
                    self.attr(attr, attr1);
            });
        };

        function tabNotifier(obj) {

            if (forceMode) {
                tabForce(obj);
                return;
            }

            switch (obj) {

            case "presentations":

                if ($('.panel-btn#tab-presentations-li').hasClass('active')) {
                    return;
                }

                $('.panel-btn#tab-presentations-li #tab-presentations > img').attr("src","img/icon/pdf_orange.png");
                break;

            case "wb":

                if ($('.panel-btn#tab-wb-li').hasClass('active')) {
                    return;
                }

                $('.panel-btn#tab-wb-li #tab-whiteboard > img').attr("src","img/icon/brush_orange.png");
                break;

            case "poll":

                if ($('#tab-polling .accordion-toggle').hasClass('active'))
                    return;
                $('#tab-polling .accordion-toggle').click();
                break;

            case "desktop":

                break;

            case "audiovideo":

                break;

            case "chat":
                if ($('#publicchat .accordion-toggle').hasClass('active'))
                    return;

                wait_msg++;
                $("#publicchat .badge").text(wait_msg);
                if ($("#publicchat .badge").hasClass("not-displayed")) $("#publicchat .badge").removeClass("not-displayed");
                break;

            }

        }

        function poll_color(reset) {
            reset = (typeof reset === "undefined") ? false : true;
            if(typeof(this.i) == 'undefined')	this.i=1;
            if (reset) this.i = 0;
            var colors = [
                          '#4572A7',
                          '#AA4643',
                          '#89A54E',
                          '#80699B',
                          '#3D96AE',
                          '#DB843D',
                          '#92A8CD',
                          '#A47D7C',
                          '#B5CA92'
                          ];
            poll_color_var = colors[this.i];
            this.i = (this.i>=8) ? 0 : ++this.i;
            return poll_color_var;
        }


        function showSlide() {

            if (screenShowed=="presentations" || switching)
                return true;

            switching = true;

            $(".wrapper").children().each(function () {
                $(this).fadeOut();
            });

            setTimeout(function () {
                $("#presentations").fadeIn();
                screenShowed = "presentations";
                switching = false;
            },1000);
        }


        function showWb() {

            if (screenShowed=="whiteboard" || switching)
                return true;

            switching = true;

            $(".wrapper").children().each(function () {
                $(this).fadeOut();
            });


            setTimeout(function () {
                console.log("AAAAAAAAA");
                $("#whiteboard").fadeIn();
                screenShowed = "whiteboard";
                switching = false;
                element_resize("wb");
            },1000);
        }

        function addVideo(path, id, startMark, endMark, teacher) {

            console.log("adding video : "+path+" ("+id+") --> " + teacher);
            availableStreams++;

            console.warn(id + " must be loaded and fires a VideoWaiting");
            $.event.trigger({
                type: "VideoWaiting",
            });

            var video = '<video id="'+id+'" preload class="video-student-content">';
            video += '<source src="'+path+'" type="video/webm" codecs="vp8, vorbis"/>';
        //	video += '<img src="img/novideo.jpg" alt="No HTML5 video support"/>'
        //		+'title="No video playback capabilities, please download the video below" />';
            /*Subs *EXP* */
        //	video += '<track src="'+path.substring(0, path.lastIndexOf("/"))+'/addsubs.vtt" kind="subtitle" srclang="en-US" label="English" />';
            video +='</video>';

            //Show Video
            appElement = "#videoStudent";
        //	if (teacher) appElement = "#videoTeacher";
            if (isEmpty($('#videoTeacher'))) appElement = "#videoTeacher";
            $(appElement).append(video);

            var webm = "" !== $(appElement).find("video").get(0).canPlayType( 'video/webm; codecs="vp8, vorbis"' );

            if(!webm) {
                errorMessage(no_video_title, no_video_text, function() { location.href='http://ietf91.conf.meetecho.com/'; });
            }



            /*Subs *EXP* */
            $($("#"+id).prop('textTracks')).prop('mode', 'hidden'); //Disabled By Default


            $("#"+id).get(0).stat = "block";

        //	if (teacher)
            streamsMapFlat[id] = new Array($("#"+id), startMark);  //MOD SFRANCISCO
            if (loadPolicy==0 || loadPolicy==1) {
                if (!isEmpty($('#videoTeacher')))
                    setTimeout(function () {
                        $("#"+id).fadeIn('fast', element_resize("teacher"));
                    }, 1000);

                if (playing)
                    streamsMapFlat[id][0].get(0).play();

                streamsMapFlat[id][0].on('canplay', function () {
                    disposeControls(streamsMapFlat[id][0], startMark, endMark, teacher);   //MOD HONOLULU1
                    if (needReseek(streamsMapFlat[id][0], parseInt(seekTime), parseInt(startMark)))
                        streamsMapFlat[id][0].get(0).currentTime = parseInt(seekTime)-parseInt(startMark);

                        $video = streamsMapFlat[id][0];

                        if ($video.get(0).stat!=null && playing) {
//                            $video.get(0).stat = "buffering";

//                          if (typeof bufferCheck[$video.attr('id')] === 'undefined')
//                             bufferCheck[$video.attr('id')] = setInterval(function () {
//                                buff = $video.get(0).buffered;
//                                if ($video.get(0).buffered.length>0 && $video.length) {
//                                    console.warn("Buff for video " + $video.attr('id') + " is " + buff.end(buff.length-1) + " " + $video.get(0).currentTime);
//                                    if (buff.end(buff.length-1)>$video.get(0).currentTime+10) {
//                                        console.warn($video.attr('id') + " is ready and fires a VideoPlaying, the stat is " + $video.get(0).stat);
//
//                                        if (waitingStreams==0) {
//                                            console.warn($video.attr('id') + " says: all is playing, nobody loves me! i'll suicide 1!");
//                                        }
//
//                                        $.event.trigger({
//                                            type: "VideoPlaying",
//                                        });
//                                        $video.get(0).stat = null;
//                                        clearInterval(bufferCheck[$video.attr('id')]);
//                                    }
//                                } else {
//                                    if (waitingStreams==0) {
//                                        console.warn($video.attr('id') + " says: all is playing, nobody loves me! i'll suicide 2!");
//                                    }
//                                    $.event.trigger({
//                                        type: "VideoPlaying",
//                                    });
//                                    $video.get(0).stat = null;
//                                    clearInterval(bufferCheck[$video.attr('id')]);
//                                    console.warn("bf check is " + bufferCheck[$video.attr('id')]);
//                                }
//
//                            }, 300);
                        } else {
                            if (waitingStreams==0)
                                console.warn($video.attr('id') + " says: all is playing, nobody loves me! i'll suicide 1!");

                            if ($(this).length)
                                $.event.trigger({
                                    type: "VideoPlaying",
                                });

                            $video.get(0).stat = null;
                            clearInterval(bufferCheck[$video.attr('id')]);
                            console.warn("bf check is " + bufferCheck[$video.attr('id')]);
                        }
                });

        //		disposeControls(streamsMapFlat[id][0], startMark, teacher);   //MOD HONOLULU1
            } else {
                streamsMapFlat[id][0].hide();
                console.log("HIDING");
            }


        }

        function addVideoSlide(path, id, startMark, endMark, teacher) {

            console.log("adding video Slide: "+path+" ("+id+") --> " + teacher);
            availableStreams++;

            console.warn(id + " must be loaded and fires a VideoWaiting");
            $.event.trigger({
                type: "VideoWaiting",
            });

            var video = '<video id="'+id+'" preload class="video-slide-content">';
            video += '<source src="'+path+'" type="video/webm" codecs="vp8, vorbis"/>';
        //	video += '<img src="img/novideo.jpg" alt="No HTML5 video support"/>'
        //		+'title="No video playback capabilities, please download the video below" />';
            video +='</video>';
            video += '<canvas id="'+id+'_canvas" width="'+($(".wrapper").width())+'" height="'+($(".wrapper").width()*3/4)+'" ></canvas>';

            //Show Video
            appElement = "#presentations";
        //	if (teacher) appElement = "#videoTeacher";
            $(appElement).append(video);

            if(!Modernizr.video) {
                errorMessage(no_video_title, no_video_text, function() { location.href='http://ietf91.conf.meetecho.com/'; });
            }

            streamsMapFlat[id] = new Array($("#"+id), startMark);  //MOD SFRANCISCO
            $("#"+id).get(0).stat = 'block';

            $("#"+id).on("loadeddata", function () {
                element_resize("all");
                $("#"+id+"_canvas").get(0).getContext('2d').drawImage($("#"+id).get(0),0,0,$("#"+id+"_canvas").get(0).width,$("#"+id+"_canvas").get(0).height);
            });

            $("#"+id).on("play", function () {
                var $this = $(this).get(0); //cache
                var relCanvas = $(this).siblings("canvas").get(0);
                (function loop() {
                        try {
                            if (!$this.paused && !$this.ended) {
                                relCanvas.getContext('2d').drawImage($this,0,0,relCanvas.width,relCanvas.height);
                                canvasLoop = setTimeout(loop, 1000 / 30); // drawing at 30fps
                            }
                        } catch (err) {
                            clearInterval(canvasLoop);
                            relCanvas.remove();
                        }
                })();
            });

        //	streamsMapFlat[id] = new Array($(".video #"+id), startMark);  //MOD SFRANCISCO
            if (loadPolicy==0 || loadPolicy==1) {
                if (playing)
                    streamsMapFlat[id][0].get(0).play();

                streamsMapFlat[id][0].on('canplay', function () {
                    disposeControls(streamsMapFlat[id][0], startMark, endMark, teacher); //MOD SFRANCISCO

                    if (needReseek(streamsMapFlat[id][0], parseInt(seekTime), parseInt(startMark)))
                        streamsMapFlat[id][0].get(0).currentTime = parseInt(seekTime)-parseInt(startMark);

                $video = $("#"+id);
                if ($video.get(0).stat!=null && playing) {
//                       $video.get(0).stat = "buffering";


//                  if (typeof bufferCheck[$video.attr('id')] === 'undefined')
//                    bufferCheck[$video.attr('id')] = setInterval(function () {
//                        buff = $video.get(0).buffered;
//                        if ($video.get(0).buffered.length>0 && $video.length) {
//                            console.warn("Buff for video " + id + " is " + buff.end(buff.length-1) + " " + $video.get(0).currentTime);
//                            if (buff.end(buff.length-1)>$video.get(0).currentTime+10) {
//                            console.warn(id + " is ready and fires a VideoPlaying, the stat is " + $video.get(0).stat);
//                                $.event.trigger({
//                                    type: "VideoPlaying",
//                                });
//                                $video.get(0).stat = null;
//                                clearInterval(bufferCheck[$video.attr('id')]);
//                                console.warn("bf check is " + bufferCheck[$video.attr('id')]);
//                            }
//                        } else {
//                            $.event.trigger({
//                                type: "VideoPlaying",
//                            });
//                            $video.get(0).stat = null;
//                            clearInterval(bufferCheck[$video.attr('id')]);
//                            console.warn("bf check is " + bufferCheck[$video.attr('id')]);
//                        }
//                    }, 300);
                     } else {
                        if ($(this).length)
                            $.event.trigger({
                                type: "VideoPlaying",
                            });

                        $video.get(0).stat = null;
                        clearInterval(bufferCheck[$video.attr('id')]);
                    }
                });
        //		disposeControls(streamsMapFlat[id][0], startMark, teacher); //MOD SFRANCISCO
            } else
                streamsMapFlat[id][0].hide();

        }

        function disposeControls($video, start, end, teacher, audio) {



            if ($video.alreadyDisposed)
                return;

            console.log("Dispose Controls");
            $video.alreadyDisposed = true;

            if (typeof audio === 'undefined')
                $video.prop("muted", true);

            if (typeof audio === 'undefined')
            $video.on('ended', function () {
                if (this.stat=="block" || this.stat=="buffering") {
                    this.stat = null;
                    clearInterval(bufferCheck);

                    if ($(this).length)
                        $.event.trigger({
                            type: "VideoPlaying",
                        });

                }

                removeStreams[$(this).attr("id")];
                $("#"+$(this).attr("id")+"_canvas").remove();

                $(this).remove();
                availableStreams--;

                if (isEmpty($('#videoTeacher'))) {
                    console.log("teacher video removed, resizing leftbar?");
                    nextTalker();
                }
                element_resize();
            }).on('timeupdate', function () {

                  if ($(this).get(0).currentTime<end || end == null)
                    return true;

                  if (this.stat=="block" || this.stat=="buffering") {
                      this.stat = null;
                      clearInterval(bufferCheck);

                      if ($(this).length)
                          $.event.trigger({
                              type: "VideoPlaying",
                          });

                  }

                  removeStreams[$(this).attr("id")];
                  $("#"+$(this).attr("id")+"_canvas").remove();

                  $(this).remove();
                  availableStreams--;

                  if (isEmpty($('#videoTeacher'))) {
                      console.log("teacher video removed, resizing leftbar?");
                      nextTalker();
                  }
                  element_resize();
              });

            if (teacher || loadPolicy==1 || loadPolicy==3) {
                $video.on('seeking', function () {
                    if (this.stat!="block" && this.stat!="buffering") {
                        this.stat = "block";
                        $.event.trigger({
                            type: "VideoWaiting",
                        });
                    }
                });

                $video.on('seeked', function () {
                    if (this.stat=="have2play") {
                        this.stat = null;
                        $(this).get(0).play();
                    } else if (this.stat=="block") {
//                        this.stat = "buffering";
                          this.stat = null;
                          if ($(this).length)
                              $.event.trigger({
                                  type: "VideoPlaying",
                              });

//                      if (typeof bufferCheck[$video.attr('id')] === 'undefined')
//                        bufferCheck[$video.attr('id')] = setInterval(function () {
//                            buff = $video.get(0).buffered;
//                            console.warn("Buff for video " + $video.attr('id') + " is " + buff.end(buff.length-1) + " " + $video.get(0).currentTime);
//                            if (buff.end(buff.length-1)>$video.get(0).currentTime+10) {
//                                console.warn($video.attr('id') + " fires a VideoPlaying");
//                                $.event.trigger({
//                                    type: "VideoPlaying",
//                                });
//                                this.stat = null;
//                                clearInterval(bufferCheck[$video.attr('id')]);
//                            }
//                        }, 300);

                    }
                });



            }

        }

        function isEmpty( el ){
            return !$.trim(el.html());
        }

        function nextTalker() {
            if (isEmpty($('#videoStudent')))
                element_resize("leftbar");
            else {
                var $cloned = $("#videoStudent").find("video").first();
                $("#videoTeacher").append($cloned);
                $("#videoStudent").find("video").first().hide();
                $cloned.get(0).play();
            }
        }


        function draw(v,c,w,h) {
            if(v.paused || v.ended) return false;
            c.drawImage(v,0,0,w,h);
            setTimeout(draw,20,v,c,w,h);
        }
