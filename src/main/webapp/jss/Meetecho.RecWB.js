var whiteboard_ongoing = false;
var wbSet = false;
var max_ntab = 5;
var conference; 

function startWB() {
		$("#whiteboard").load("template_docebo/wb_html5.html", function () {
			conference = new Conference(""); // istanzio l'oggetto conferenza 
			addTab(0);
			console.log("first call to showWb");
			whiteboard_ongoing = true;
			element_resize("wb");
		});
		wbSet = true;
}


function checkIfMoreLineTab() {
	var more_line = ($('#wbtabs ul:not(.picker_nav) li:not(.newTab)').last().index()>max_ntab) ? true : false;
	if (more_line) $('#wbtabs ul:not(.picker_nav)').css("overflow-y","auto");
	else $('#wbtabs ul:not(.picker_nav)').css("overflow-y","hidden");
	return true;
}

function addTab(id) {

	// Dimensioni necessario usare le temp per definire la dimensione dei canvas prima dell'istanziazione dell'oggetto
	if ($("#meetecho-wbtab.nav-tabs").width()==0) {
		$("#meetecho-wbtab.nav-tabs").width($("#wbtabs").width());
	}
	
	var dimtemp_h = $("#wbtabs").height()-$("#meetecho-wbtab.nav-tabs").height()-2;
	var dimtemp_w = (dimtemp_h)*4/3;//$(".tab-content").width()-2;

	tabContentHtml = "<div id='tab-" + id + "' class='tab-pane canvasTab'><canvas class='canvas' id='c"+id+"' width='1200' height='750'></canvas></div>"; // Contenuto del tab
	tabHeaderHtml = "<li><a href='#tab-"+id+"'>"+wb_obj_name+" "+id+"</a></li>";

	$('.nav-tabs').append(tabHeaderHtml);
	$('.tab-content').append(tabContentHtml);
	$(".canvasTab:last").height(dimtemp_h).width(dimtemp_w);

	$(".canvasTab.active").removeClass("active");
	$("#tab-"+id).addClass("active");		
	//controllo se si ci sono pi� di 5 slide
	//in caso affermativo imposto owerflow-y ad auto, altrimenti lo imposto a hidden
	checkIfMoreLineTab();

	// Aggiungo l'oggetto WhiteBoard alle Whiteboard della conferenza
	current_wb = new WhiteBoard($("#c"+id)[0],id+1); // Imposta current_wb con la nuova whitebord 
	conference.wb.push(current_wb); // Aggiunge la nuova Whiteboard alla collezione
	
	initTab(id);	
}

function initTab(tab) {
	console.log("Initializing WB tabs... :" +tab);
	// Inizializzo
	$("[href='#tab-"+tab+"']").on('shown', function() {
		var indice_wb = $(this).parent().index();
		console.log(indice_wb);
		current_wb = conference.wb[indice_wb];
		console.log("Selected WB " + tab + " (" + indice_wb + ")");
		$(".canvasTab.active").removeClass("active");
		$("#tab-"+tab).addClass("active");		
	});
			
	$('#wbtabs ul:not(.picker_nav) li:not(.newTab)').last().on('click', function() {
		$(this).find("a").tab('show');
	});
	
	$("[href='#tab-"+tab+"']").closest("li").trigger('click');	// Simula il click per selezionare la nuova WhiteBoard
}

/**********************************************************************************
 * Classe Conference
 * 
 * Questa classe rappresenta la Conferenza per la quale � richiesto l'uso della 
 * whiteboard. Contiene lo stato di tutte le whiteboard e metodi per gestire la
 * connessione e lo scambio di messaggi.
 * 
 **********************************************************************************/
function Conference (_id) {
	this.wb_count = 1; 
	this.wb = [];
	this.id = _id;
	this.isClosed = false;
	this.sid;
}

/**
 *  Effettua la long polling tramite una richiesta AJAX
 */
Conference.prototype.startPoll = function() {
	var data = getRandomId() + "action=joinWb&type=html5";
	$.ajax({
		type: 'POST',
		url: "./Handler",
		cache: false,
		contentType: "application/x-www-form-urlencoded;charset=UTF-8",
		data: data,
		success: function(xml) {
			// Ok
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			errorMessage("joinwb", textStatus + ": " + errorThrown);	// FIXME
		},
		dataType: "xml"
	});
};

/**
 * Rollback create new whiteboard
 */

Conference.prototype.closeTab = function () {
	_this = this;
	_this.wb_count--;
	$("#tab-"+_this.wb_count).remove();
	console.log("Removing "+_this.wb_count);
	$("##tab-"+_this.wb_count).closest("li").remove();
	var idx = $(this).parent().siblings("a").attr("href").split("-")[1];//recupero l'indice
	checkIfMoreLineTab();
	conference.wb.splice(idx,1); // elimina la whiteboard dalla collezione
	$('#meetecho-wbtab').find("a:last").trigger('click');	// Simula il click per selezionare l'ultima tab aperta
}

/**
 *  Gestisce il messaggio ricevuto ed effettua le operazioni sulle whiteboard.
 */
Conference.prototype.receiveIQ = function(msg) { 
    _this = this; // riferimento alla Conference
    $(msg).find('query').each(function() { // per ogni tag <query>
    	var command = $(this).find('command').text(); // il comando inviato
    	var wboardID = $(this).find('wboardID').text(); // l'id della whiteboard
    	var wboard = _this.getWBbyID(wboardID); // recupero l'oggetto whitebord
    	var msgAuthor = $(this).find('author').text(); // l'autore del messaggio
    	console.log("new draw on WB ");
    	console.log(wboardID+"-"+wboard+"-");
    	
    	switch(command) { // a seconda del comando inviato
			case "name" : // rinomino un tab
				var tabRef = wboardID-1;
				console.log("tabref = ",tabRef);
				if(wboard!=null) {
					var name  = $(this).find('label').text();
					wboard.rename(name);
					$('#wbtabs > .tab-content').find('.tab-pane').each(function(k) {
						if($(this).attr("id")=="tab-"+tabRef) {
							$($('#wbtabs').find('a')[k]).text(name);
						}
					});
				}
				break;
		case "new" : // nuova whiteboard
			/*
			 *  TODO : Se ricevo il messaggio di new che hi inviato io che faccio? 
			 *  
			 *  Ora lo uso se no non funziona il rientro ad una conferenza
			 *  
			 */
		//if (msgAuthor!=author) { // Se ricevo il msg new inviato da me lo scarto
			addTab(_this.wb_count);
			_this.wb_count++;
		//}
			break;
		case "undo" : 
			if(wboard!=null) 
				wboard.unDo(msgAuthor);
			break;
		case "clear" : 
			if(wboard!=null) 
				wboard.clear();
			break;
		case "image" :
			var imgB64 = $(this).find('image').text();
			var type = $(this).find('file_type').text();
			if(wboard!=null) 
				wboard.setBG(imgB64,type,msgAuthor);
			break;
		case "write" :
			var figure = $(this).find('figure');
			var figure_type = $(figure).attr("object");
			var intcolor = $(this).find('color').text();
			var color = null;
			if(intcolor === '0')
				color = 'black';
			else
				color = parseInt(intcolor).toString(16);
			var dotWidth = parseFloat($(this).find('dotWidth').text());
			var dash = $(this).find('dash').text() == "true" ? true : false;
			var objID = $(this).find('wbPacketID').text();

			if(wboard!=null) { 
				// Se ricevo un messaggio che contiene un id di una figura gi� 
                                // presente per evitare problemi con la write la distruggo e ricreo
				wboard.removeShapeByID(objID);
			
				switch (figure_type) {
					case "ELLIPSE" :
						var x1 = parseFloat($(figure).find('x1').text());
						var y1 = parseFloat($(figure).find('y1').text());
						var x2 = parseFloat($(figure).find('x2').text());
						var y2 = parseFloat($(figure).find('y2').text());
						var beFill = $(figure).find('beFill').text()== "true" ? true : false;
						wboard.addShape(new Ellipse(x1,y1,x2,y2,beFill,color,dotWidth,dash,msgAuthor,objID));
						break;					
					case "RECT" :
						var x1 = parseFloat($(figure).find('x1').text());
						var y1 = parseFloat($(figure).find('y1').text());
						var x2 = parseFloat($(figure).find('x2').text());
						var y2 = parseFloat($(figure).find('y2').text());
						var beFill = $(figure).find('beFill').text()== "true" ? true : false;
						wboard.addShape(new Rect(x1,y1,x2,y2,beFill,color,dotWidth,dash,msgAuthor,objID));
						break;
					case "ARROW" :
						var x1 = parseFloat($(figure).find('x1').text());
						var y1 = parseFloat($(figure).find('y1').text());
						var x2 = parseFloat($(figure).find('x2').text());
						var y2 = parseFloat($(figure).find('y2').text());
						wboard.addShape(new Arrow(x1,y1,x2,y2,color,dotWidth,dash,msgAuthor,objID));
						break;			
					case "LINE" :
						var x1 = parseFloat($(figure).find('x1').text());
						var y1 = parseFloat($(figure).find('y1').text());
						var x2 = parseFloat($(figure).find('x2').text());
						var y2 = parseFloat($(figure).find('y2').text());
						wboard.addShape(new Line(x1,y1,x2,y2,color,dotWidth,dash,msgAuthor,objID));
						break;					
					case "PENCIL" :
						var xv = $(figure).find('vector_X').text().split(",").map(parseFloat);
						var yv = $(figure).find('vector_Y').text().split(",").map(parseFloat);
						wboard.addShape(new Path(xv,yv,color,dotWidth,dash,msgAuthor,objID));
						break;			
					case "RUBBER" :
						var xv = $(figure).find('vector_X').text().split(",").map(parseFloat);
						var yv = $(figure).find('vector_Y').text().split(",").map(parseFloat);
						wboard.addShape(new Rubber(xv,yv,color,dotWidth,dash,msgAuthor,objID));
						break;			
					case "CLIP_ART" :
						var x = parseFloat($(figure).find('x').text());
						var y = parseFloat($(figure).find('y').text());
						var x2 = parseFloat($(figure).find('x2').text());
						var y2 = parseFloat($(figure).find('y2').text());
						var imgB64 = $(figure).find('image').text();
						var type = $(figure).find('file_type').text();
						var tShape = new Img(x,y,type,msgAuthor,objID);
						tShape.resize(x2, y2);
						wboard.addShape(tShape);
						var img = new Image();
						img.onload = function() {
							tShape.setImg(img, imgB64);
							wboard.dirty = true;
						};
						img.src = "data:image/"+type+";base64,"+imgB64;
						break;		
					case "TEXT" :
						var x = parseFloat($(figure).find('x').text());
						var y = parseFloat($(figure).find('y').text());
						var text = $(figure).find('text').text();
						var obj = wboard.getObjbyID(objID);
						if (obj) {
							obj.text = text;
							obj.size = dotWhidth;
							obj.color = color;
						} else 
							wboard.addShape(new Text(x,y,text, color, dotWidth, "Calibri", msgAuthor,objID));	
						break;
					case "LASER" :
						var x = parseFloat($(figure).find('x').text());
						var y = parseFloat($(figure).find('y').text());
						var radius = parseFloat($(figure).find('radius').text());
						wboard.removeLaser(msgAuthor);
						if (!(x==-1 && y==-1)) 
							wboard.addLaser(new Laser(x,y,color,radius,msgAuthor,objID));
						break;					
					case "HIGHLIGHT" :
						var x1 = parseFloat($(figure).find('x1').text());
						var y1 = parseFloat($(figure).find('y1').text());
						var x2 = parseFloat($(figure).find('x2').text());
						var y2 = parseFloat($(figure).find('y2').text());
						var beFill = $(figure).find('beFill').text()== "true" ? true : false;
						wboard.addShape(new Highlight(x1,y1,x2,y2,beFill,color,dotWidth,dash,msgAuthor,objID));
						break;				
				}
			}
			

			break;
			
		case "move":
		case "resize":
		case "update":
			// Rispesco tutti i valori ma in realt� ne uso solo alcuni
			var figure = $(this).find('figure');
			var figure_type = $(figure).attr("object");
//			var intcolor = $(this).find('color').text();
//			var color = null;
//			if(intcolor === '0')
//				color = 'black';
//			else
//				color = parseInt(intcolor).toString(16);
//			var dotWidth = parseFloat($(this).find('dotWidth').text());
//			var dash = $(this).find('dash').text() == "true" ? true : false;
			var objID = $(this).find('wbPacketID').text();
			if (wboard != null) {
				var obj = wboard.getObjbyID(objID);
				switch (figure_type) {
				case "ELLIPSE" :
				case "RECT" :
				case "ARROW" :		
				case "LINE" :
				case "HIGHLIGHT" :
					var x1 = parseFloat($(figure).find('x1').text());
					var y1 = parseFloat($(figure).find('y1').text());
					var x2 = parseFloat($(figure).find('x2').text());
					var y2 = parseFloat($(figure).find('y2').text());
					if (obj) {
						obj.x1 = x1;
						obj.y1 = y1;
						obj.resize(x2,y2);
					}
					break;					
				case "CLIP_ART" :
					var x = parseFloat($(figure).find('x').text());
					var y = parseFloat($(figure).find('y').text());
					var x2 = parseFloat($(figure).find('x2').text());
					var y2 = parseFloat($(figure).find('y2').text());
					if (obj) {
						obj.x1 = x;
						obj.y1 = y;
						obj.resize(x2,y2);
					}
					break;		
				case "TEXT" :
					var x = parseFloat($(figure).find('x').text());
					var y = parseFloat($(figure).find('y').text());
					if (obj) {
						obj.x1 = x;
						obj.y1 = y;
					}
					break;
					
				}
				for (var i=wboard.shapes.length-1; i>=0; i--) 	  			
					if (objID==wboard.shapes[i].id) {
					wboard.shapes.splice(i,1);
					wboard.shapes.push(obj);
					}
				wboard.dirty = true;	
			}
			break;
		}
//    	$('#tab-'+wboardID).getNiceScroll().resize();
	});	
	return true;
	
};

/**
 * Recupera la whiteboard con specifico id
 */
Conference.prototype.getWBbyID = function(_id) {
	for (var i = 0; i<this.wb.length;i++)
		if (this.wb[i].id == _id) return this.wb[i];
	return null;
};

function b64parse(text){
    text = text.replace(/ /g, '+');
    console.log(text);
    return text;
}