var fakeid = 0, systemLast = false, lastVisible = true;




//Chat display helper
function hashCode(str) { // java String#hashCode
	var hash = 0;
	for (var i = 0; i < str.length; i++) {
		hash = str.charCodeAt(i) + ((hash << 5) - hash);
	}
	return hash;
} 

function addChatMsg(user, message, id, time) {
	if(user === null || user === '')
		return;
	if(id === null || id === '')
		id = "fakeID_"+fakeid++;
	if(message === null || message === '')
		return;
	
//	message = clearText(message);
	
	var nowTime = "" + time;
	if(user === '***') {
//		$('#chat').append("<div id="+id+" class='chat-msg-system'><span class='chat-text'>"+ nowTime + " - " + chat_string_create_urls(replaceSmilies(message)) + "</span></div>");
//		systemLast = true;
	} else if(user ==='<<<') {
		$('#chat').append("<div id="+id+" class='chat-msg-system'><span class='chat-text'>" + chat_string_create_urls(replaceSmilies(message)) + "</span></div>");
		systemLast = true;
	} else if(user ==='[[[') {
		$('#chat').append("<div id="+id+" class='chat-msg-system'><span class='chat-text'>"+ nowTime + " - " + chat_string_create_urls(replaceSmilies(message)) + "</span></div>");
		systemLast = true;
	} else if(user ===']]]') {
		$('#chat').append("<div id="+id+" class='chat-msg-system'><span class='chat-text'>"+ nowTime + " - " + chat_string_create_urls(replaceSmilies(message)) + "</span></div>");
		systemLast = true;
	} else if(user ==='###') {
		$('#chat').append("<div id="+id+" class='chat-msg-system'><span class='chat-text'>"+ nowTime + " - " + chat_string_create_urls(replaceSmilies(message)) + "</span></div>");
		systemLast = true;
	} else {
		/*previous code*/
		//var color = "rgb(98, 126, 199)"; //c'era l'hash del nome! FIXME
		//$('#chat').append("<div id="+id+" class='chat-msg'><div class='chat-header'><span class='chat-talker' style='color:"+color+"'>"+user+"</span><span class='chat-time'>"+nowTime+"</span></div><span class='chat-text'>"+ message + "</span></div>");
		/*end previous*/
		var $last_talk = $("#chat .chat-msg").last();
		if ($last_talk.find(".chat-talker").text()==user && !systemLast) {
			var color = "rgb(255, 255, 255)";
			$last_talk.append("<div id="+id+" class='chat-msg'><span hidden class='chat-talker' style=\"color:"+color+";  font-size: 0pt;\">"+user+"</span><table border=\"0\" width=100%><tr><td><span class='chat-text'>"+ chat_string_create_urls(replaceSmilies(message))+"</span></td><td valign=\"top\"><span class='chat-time'>"+nowTime+"</span></td></tr></table></div>");
		} else {
			var color = "rgb(98, 126, 199)"; //c'era l'hash del nome! FIXME
			var colorFont = "rgb(255, 255, 255)";
			$('#chat').append("<div id="+id+" class='chat-msg'><div class='chat-header'><span class='chat-talker' style='color:"+color+"'>"+user+"</span><span hidden class='chat-time'>"+nowTime+"</span></div><table border=\"0\" width=100%><tr><td><span class='chat-text'>"+ chat_string_create_urls(replaceSmilies(message))+"</span></td><td valign=\"top\"><span class='chat-time'>"+nowTime+"</span></td></tr></table></div>");
		}
		systemLast = false;
	}

//	$('#chatbox').animate({ scrollTop: $('#chat').height() }, "fast");

	$("#chatbox").each(function(i) {
		// certain browsers have a bug such that scrollHeight is too small
		// when content does not fill the client area of the element
		var scrollHeight = Math.max(this.scrollHeight, this.clientHeight);
		if (lastVisible)
		this.scrollTop = scrollHeight - this.clientHeight;
	});

	tabNotifier("chat");
}


function intToARGB(i){
	return ((i>>24)&0xFF).toString(16) + 
	((i>>16)&0xFF).toString(16) + 
	((i>>8)&0xFF).toString(16);
}

function chat_string_create_urls(input) {
    input = input.replace("&lt;", "<");
    input = input.replace("&gt;", ">");
    return input.replace(/(ftp|http|https|file):\/\/[\S]+(\b|$)/gim, '<a href="$&" target="_blank" title="$&">$&</a>')
    .replace(/([^\/])(www[\S]+(\b|$))/gim, '$1<a href="http://$2" target="_blank" title="$2">$2</a>');
} 

function replaceSmilies(input) {
	return input
        .replace(/:s/g, '<img src="smilies/cold_sweat.png" style="height:20px"></img>')
        .replace(/:S/g, '<img src="smilies/cold_sweat.png" style="height:20px"></img>')
        .replace(/8\)/g, '<img src="smilies/alien.png" style="height:20px"></img>')
        .replace(/8-\)/g, '<img src="smilies/alien.png" style="height:20px"></img>')
        .replace(/:\)/g, '<img src="smilies/smile.png" style="height:20px"></img>')
        .replace(/:-\)/g, '<img src="smilies/smile.png" style="height:20px"></img>')
        .replace(/:D/g, '<img src="smilies/grin.png" style="height:20px"></img>')
        .replace(/:-D/g, '<img src="smilies/grin.png" style="height:20px"></img>')
        .replace(/xD/g, '<img src="smilies/grin.png" style="height:20px"></img>')
        .replace(/XD/g, '<img src="smilies/grin.png" style="height:20px"></img>')
        .replace(/:\\/g, '<img src="smilies/line_mouth.png" style="height:20px"></img>')
        .replace(/:-\\/g, '<img src="smilies/line_mouth.png" style="height:20px"></img>')
        .replace(/:\|/g, '<img src="smilies/line_mouth.png" style="height:20px"></img>')
        .replace(/:-\|/g, '<img src="smilies/line_mouth.png" style="height:20px"></img>')
        .replace(/:-\//g, '<img src="smilies/line_mouth.png" style="height:20px"></img>')
        .replace(/:\(/g, '<img src="smilies/cry.png" style="height:20px"></img>')
        .replace(/:-\(/g, '<img src="smilies/cry.png" style="height:20px"></img>')
        .replace(/\>\(/g, '<img src="smilies/angers.png" style="height:20px"></img>')
        .replace(/\>:\|/g, '<img src="smilies/angers.png" style="height:20px"></img>')
        .replace(/\>:-\|/g, '<img src="smilies/angers.png" style="height:20px"></img>')
        .replace(/\>:\(/g, '<img src="smilies/angers.png" style="height:20px"></img>')
        .replace(/\>:-\(/g, '<img src="smilies/angers.png" style="height:20px"></img>')
        .replace(/:\*/g, '<img src="smilies/heart_eyes.png" style="height:20px"></img>')
        .replace(/:-\*/g, '<img src="smilies/heart_eyes.png" style="height:20px"></img>')
        .replace(/:o/g, '<img src="smilies/open_mouth.png" style="height:20px"></img>')
        .replace(/:-o/g, '<img src="smilies/open_mouth.png" style="height:20px"></img>')
        .replace(/:O/g, '<img src="smilies/open_mouth.png" style="height:20px"></img>')
        .replace(/:-O/g, '<img src="smilies/open_mouth.png" style="height:20px"></img>')
        .replace(/:p/g, '<img src="smilies/stuck_out_tongue.png" style="height:20px"></img>')
        .replace(/:-p/g, '<img src="smilies/stuck_out_tongue.png" style="height:20px"></img>')
        .replace(/:P/g, '<img src="smilies/stuck_out_tongue.png" style="height:20px"></img>')
        .replace(/:-P/g, '<img src="smilies/stuck_out_tongue.png" style="height:20px"></img>')
        .replace(/;\)/g, '<img src="smilies/blink.png" style="height:20px"></img>')
        .replace(/;-\)/g, '<img src="smilies/blink.png" style="height:20px"></img>')
        .replace(/;D/g, '<img src="smilies/blink.png" style="height:20px"></img>')
        .replace(/\(Y\)/g, '<img src="smilies/thmbup.png" style="height:20px"></img>')
        .replace(/\(y\)/g, '<img src="smilies/thmbup.png" style="height:20px"></img>')
        .replace(/\(N\)/g, '<img src="smilies/thmbdwn.png" style="height:20px"></img>')
        .replace(/\(n\)/g, '<img src="smilies/thmbdwn.png" style="height:20px"></img>')
        .replace(/;-D/g, '<img src="smilies/blink.png" style="height:20px"></img>');
}

function setUpChat() {
	$('#chatbox').bind('scroll', function() {
		if($(this).scrollTop() + $(this).innerHeight()>= $(this)[0].scrollHeight)		
			lastVisible = true;
		else 
			lastVisible = false;			
	});
}

function clearText(element) {
	return element.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/"/g, '&quot;');
}