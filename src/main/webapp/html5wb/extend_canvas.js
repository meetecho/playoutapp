/************************************************************************************
 *  
 * extend_canvas.js
 * 
 * Metodi aggiuntivi per il canvas
 * I metodi seguenti aggiungono al canvas due nuove funzionalità :
 * - dashedLineTo : disegna una linea tratteggiata
 * - dasehdbezierCurveTo : disegna una curva bezier tratteggiata
 * 
 * Fonti :
 * http://davidowens.wordpress.com/2010/09/07/html-5-canvas-and-dashed-lines/
 * http://stackoverflow.com/questions/7352769/dashed-curves-on-html5-canvas-bezier
 * http://it.wikipedia.org/wiki/Curva_di_B%C3%A9zier#Applicazioni_nella_computer_grafica
 *
 *  @author Claudio Perrotta <c.perrotta@studenti.unina.it>,<cl.perrotta@gmail.com>
 *
 ************************************************************************************/

// dashedLineTo : disegna una linea tratteggiata composta da tanti segmenti, 
// utilizzando un pattern per alternare la lunghezza dei tratti e degli spazi bianchi
// es. [3,4] 		->	 ---  ---  ---  ---
// es. [3,2,1,2] 	->	 ---  -  ---  -  ---
CanvasRenderingContext2D.prototype.dashedLineTo = function (fromX, fromY, toX, toY, pattern) {
	
	// Funzioni di utilità
	var lt = function (a, b) { return a <= b; }; 
	var gt = function (a, b) { return a >= b; };
	var capmin = function (a, b) { return Math.min(a, b); };
	var capmax = function (a, b) { return Math.max(a, b); };
	
	var checkX = { thereYet: gt, cap: capmin }; // Variabile per il controllo delle ascisse
	var checkY = { thereYet: gt, cap: capmin }; // Variabile per il controllo delle ordinate

	// teniamo in conto della direzione
	if (fromY - toY > 0) { 
		checkY.thereYet = lt;
		checkY.cap = capmax;
	}
	if (fromX - toX > 0) { 
		checkX.thereYet = lt;
		checkX.cap = capmax;
	}
	
	this.moveTo(fromX, fromY); // primo punto
	
	//offset inizale
	var offsetX = fromX; 
	var offsetY = fromY;
	
	var idx = 0; // indice del pattern
	var dash = true; // controllo dash
	
	while (!(checkX.thereYet(offsetX, toX) && checkY.thereYet(offsetY, toY))) { // se c'è ancora da disegnare
		
		var ang = Math.atan2(toY - fromY, toX - fromX); // angolo della linea
		var len = pattern[idx]; // lunghezza tratto

		// Calcolo nuovi offset per le coordinate
		offsetX = checkX.cap(toX, offsetX + (Math.cos(ang) * len)); 
		offsetY = checkY.cap(toY, offsetY + (Math.sin(ang) * len)); 

		if (dash) this.lineTo(offsetX, offsetY);
		else this.moveTo(offsetX, offsetY);

		// itera il pattern
		idx = (idx + 1) % pattern.length; 
		dash = !dash; 
	}
};

// dashedBezierCurveTo : disegna una curva bezier tratteggiata composta da tanti archi,
// questi sono ottenuti tramite interpolazione grazie alla funzione bezier.
// Anche qui utilizziamo un pattern per alternare la lunghezza dei tratti e degli spazi bianchi
// es. [3,2] 		->	 ---  ---  ---  ---
// es. [3,2,1,2] 	->	 ---  -  ---  -  ---
//
// TODO : come impostare lo step?!?!
CanvasRenderingContext2D.prototype.dashedBezierCurveTo = function (fromX, fromY,controlX1, controlY1, controlX2, controlY2, toX, toY, pattern) {

	// Funzione di utilità che calcola il punto p(x,y) della curva in relazione a t
	// in ingresso prende un vettore di punti [x1,y1,x2,y2,...,xn,yn] che rappresenta i punti di controllo
	var bezier = function(cp, t) {
		var ax, bx, cx;
		var ay, by, cy;
		var tSquared, tCubed;
		var resultx, resulty;

		// calcolo dei coefficienti del polinomio
		cx = 3.0 * (cp[2] - cp[0]);
		bx = 3.0 * (cp[4] - cp[2]) - cx;
		ax = cp[6] - cp[0] - cx - bx;
	
		cy = 3.0 * (cp[3] - cp[1]);
		by = 3.0 * (cp[5] - cp[3]) - cy;
		ay = cp[7] - cp[1] - cy - by;
	
		// calcolo del punto della curva in relazione a t
		tSquared = t * t;
		tCubed = tSquared * t;
		resultx = (ax * tCubed) + (bx * tSquared) + (cx * t) + cp[0];
		resulty = (ay * tCubed) + (by * tSquared) + (cy * t) + cp[1];

		return [resultx,resulty];
	};
    

	var step = 0.001; // Step utilizzato per campionare la curva
	var subPaths = []; // collezione di tratti
	var loc = bezier([fromX, fromY,controlX1, controlY1, controlX2, controlY2, toX, toY], 0); // primo punto
	var lastLoc = loc;

	var dashIndex = 0; // indice del pattern
	var length = 0; // lunghezza del tratto
	var thisPath = []; // tratto corrente
	for(var t = step; t <= 1; t += step) { // campionamento
		loc = bezier([fromX, fromY,controlX1, controlY1, controlX2, controlY2, toX, toY], t); // campione per 0 <= t <= 1
	
		var d = [loc[0] - lastLoc[0], loc[1] - lastLoc[1]]; // delta
		length += Math.sqrt(d[0]*d[0] + d[1] * d[1]);  // lunghezza dell'arco
		lastLoc = loc;
	
		// Rileviamo se siamo alla fine di uno spazio vuoto o tratto
		if(length >= pattern[dashIndex]) {

			//Se siamo nel tratto allora dobbiamo aggiungere il tratto corrente alla collezione
			if(dashIndex % 2 == 0)
				subPaths.push(thisPath);

			// aggiorniamo l'indice del pattern
			dashIndex = (dashIndex + 1) % pattern.length;
      
			// Reinizzializiamo le variabili
			thisPath = [];
			length = 0;
		}

		//Se siamo ancora in un tratto aggiungiamo il punto al tratto corrente.
		if(dashIndex % 2 == 0) 
			thisPath.push(loc[0], loc[1]);
		
	}
	
	// aggiungiamo l'ultimo tratto
	if(thisPath.length > 0)
		subPaths.push(thisPath);
  
	// disegniamo tutti i tratti nella collezione
	for(var i = 0; i < subPaths.length; i++) {
		var part = subPaths[i];
		if(part.length > 0)
			this.moveTo(part[0], part[1]);
		for(var j = 1; j < part.length / 2; j++) 
			this.lineTo(part[2*j], part[2*j+1]);
		
	}
};
