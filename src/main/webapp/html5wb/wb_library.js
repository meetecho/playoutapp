
/************************************************************************************
 * 
 * wb_library.js
 * 
 * In questo file sono contenute tutte le classi che si occupano del disegno effettivo
 * sul canvas.
 * Abbiamo una classe che rappresenta la generica WhiteBoard e le classe che 
 * rappresentano le singole figure da disegnare.
 * 
 * @author Claudio Perrotta 
 * <br><a href="mailto:cl.perrotta@gmail.com">cl.perrotta@gmail.com</a>
 * <br><a href="mailto:c.perrotta@studenti.unina.it">c.perrotta@studenti.unina.it<a/>
 *
 ************************************************************************************/


/********************
 * Classe WhiteBoard 
 ********************/
function WhiteBoard(canvas, id) { 
  this.canvas = canvas; // la referenza al tag <canvas>
  this.width = canvas.width; // larghezza del canvas
  this.height = canvas.height; // altezza del canvas
  this.ctx = canvas.getContext("2d"); // Inizializzazione del canvas per grafica 2d
  this.dirty = true; // Variabile che indica se il canvas e sporco e va ridisegnato
  this.shapes = [];  // Collezione contenente le figure da disegnare
  this.background; // qui viene salvato lo sfondo del canvas
  this.toolShape; // questa è la figura temporanea per l'anteprima delle operazioni di disegno
  this.selection; // questa è una referenza alla figura selezionata
  this.id = id; // id per distinguere tra le diverse whiteboard
  this.label = "Lavagna "+id;
  this.resizer = new Resizer(0,0); // oggetto utilizzato per ridimensionare le figure
  
  // Evita la selezione del testo sul canvas con un doppio click
  canvas.addEventListener('selectstart', function(e) { e.preventDefault(); return false; }, false);
  
  // Impostazione della selezione
  this.selectionColor = '#DDDDDD';
  this.selectionWidth = 2;  
  
  var _this = this; // Server per avere un handle all'oggetto nella funzione qui sotto
  // **** Setta l'aggiornamento del canvas ****
  this.interval = 30;
  setInterval(function() { _this.draw(); }, _this.interval);
  
  this.laser = []; // Collezione di oggetti laser 
}

/**
 * Recupara una figura sulla base dell'id passato come input, restituisce null se all'id non corrisponde nessuna figura nella collezione
 */
WhiteBoard.prototype.getObjbyID = function(_id) { 
	// itero per tutte le figure
	for (var i = 0; i<this.shapes.length;i++)
		if (this.shapes[i].id == _id) return this.shapes[i];
	return null;
};

/**
 * Aggiunge una figura al canvas e lo setta come dirty
 */
WhiteBoard.prototype.addShape = function(shape) {
  this.shapes.push(shape);
  this.dirty = true;
};

/**
 * Aggiunge un laser al canvas e lo setta come dirty
 */
WhiteBoard.prototype.addLaser = function(shape) {
	  this.laser.push(shape);
	  this.dirty = true;
	};

/**
 * Recupara un laser a seconda dell'autore
 */
WhiteBoard.prototype.getLaserByAuthor = function(_author) { 
	for (var i = 0; i<this.laser.length;i++)
		if (this.laser[i].author == _author) return this.laser[i];
	return null;
};

/**
 * Rimuove un laser dalla collezione sulla base dell'autore passato come parametro e setta il canvas come dirty
 */
WhiteBoard.prototype.removeLaser = function(_author) { 
	  // itera tra i laser
	  for (var i=this.laser.length-1; i>=0; i--) 
		  if (_author==this.laser[i].author) {
				this.laser.splice(i,1); // elimina l'oggetto dalla collezione
				this.dirty = true;
				return true; // per uscire dal for
		};	
};

WhiteBoard.prototype.removeShapeByID = function (_id) {
	  for (var i=this.shapes.length-1; i>=0; i--) 	  			
			if (_id==this.shapes[i].id) {
				this.shapes.splice(i,1);
				this.dirty = true;
				return true; // per uscire dal for
			}
	  return false;
};

/**
 * Rinomina la whiteboard
 */
WhiteBoard.prototype.rename = function(_label) {
	  this.shapes.label = _label;
};
		
/**
 * Pulisce il canvas
 */
WhiteBoard.prototype.clear = function() {
  this.shapes = []; 
  this.background = null;
  this.dirty = true;
};

/**
 * Annulla l'ultima figura inserita dall'autore
 */
WhiteBoard.prototype.unDo = function (_author) {
  for (var i=this.shapes.length-1; i>=0; i--) 	  			
		if (_author==this.shapes[i].author) {
			this.shapes.splice(i,1);
			this.dirty = true;
			return true; // per uscire dal for
		}
  if (this.background)
	  if (this.background.author==_author) {
		  this.background = null;
		  this.dirty = true;
		  return true; // per uscire dala funzione-
	  }
	  
  return false;
};

/**
 *  setBG : Setta lo sfondo della whiteboard 
 *  Necessita come parametri la codifica base64 dell'immagine, il file type e l'autore
 */
WhiteBoard.prototype.setBG = function (imgB64,type,author) {
	var img = new Image(); // creo un oggeto image vuoto
	var bkg = new BGImg(0,0,type,author,genericID());
	this.background = bkg; // lo setto come background
	var _this = this; // handle per this
	img.onload = function() { // al caricamento dell'immagine
		var xT,yT;
		var ratio = this.height/this.width; // ottengo l'aspect ratio dell'immagine
		if (img.height/img.width > ratio){  // se è l'altezza troppo grande
			img.width = Math.round(img.width*(_this.height/img.height)); // ridimensiono la width mantenendo l'aspect ratio
			img.height = _this.height; // setto l'height uguale a quella del canvas
//			// Setto le coordinate per centrare l'immagine
			yT = 0; 
			xT =   _this.width/2 - img.width/2;
		} else { // altrimenti se è la larghezza il problema
			img.height = Math.round(img.height*(_this.width/img.width)); // ridimensiono l'height mantenendo l'aspect ratio
			img.width = _this.width; // setto la width uguale a quella del canvas
			// Setto le coordinate per centrare l'immagine
			xT = 0;
			yT =  _this.height/2 - img.height/2; 
		} 

		
		
		//		var xT,yT;
//		var ratio = this.height/this.width; // ottengo l'aspect ratio dell'immagine
//		if (img.height/img.width > ratio){  // se è l'altezza troppo grande
//			img.width = Math.round(img.width*(_this.height/img.height)); // ridimensiono la width mantenendo l'aspect ratio
//			img.height = _this.height; // setto l'height uguale a quella del canvas
//			// Setto le coordinate per centrare l'immagine
//			yT = 0; 
//			xT =   _this.width/2 - img.width/2;
//		} else { // altrimenti se è la larghezza il problema
//			img.height = Math.round(img.height*(_this.width/img.width)); // ridimensiono l'height mantenendo l'aspect ratio
//			img.width = _this.width; // setto la width uguale a quella del canvas
//			// Setto le coordinate per centrare l'immagine
//			xT = 0;
//			yT =  _this.height/2 - img.height/2; 
//		} 
		bkg.setImg(0,0,_this.width,_this.height,img,imgB64); // Setto l'immagine

//		bkg.setImg(0,0,img.width,img.height,img,imgB64); // Setto l'immagine
//		bkg.setImg(xT,yT,img.width,img.height,img,imgB64); // Setto l'immagine
	    _this.dirty = true;
 	};
	img.src = "data:image/"+type+"; base64,"+imgB64; // imposto la source
};

	
/**
 * Disegna gli elementi sul cavans
 */
WhiteBoard.prototype.draw = function() {
  // Se il canvas è sporco lo ridisegno
  if (this.dirty) {
	this.ctx.clearRect(0, 0, this.width, this.height); // pulisco il canvas
    
	// Disegno le figure nalla collezzione shapes
    for (var i = 0; i < this.shapes.length; i++) 
      this.shapes[i].draw(this.ctx);
    
    // Se presente disegno la figura in toolShape
    if (this.toolShape) 
      this.toolShape.draw(this.ctx);
    
    // Disegno i laser presenti nella relativa collezione
    for (var i = 0; i< this.laser.length; i++) 
      this.laser[i].draw(this.ctx);
   
    // Se è stata selezionata una figura disegno un rettangolo che la contiene
    if (this.selection) {
    	this.ctx.strokeStyle = this.selectionColor;
		this.ctx.lineWidth = this.selectionWidth;
		this.ctx.strokeRect(this.selection.x1,this.selection.y1,(this.selection.x2 -  this.selection.x1),(this.selection.y2 -  this.selection.y1));
	}
    
    // Se è necessario disegno il resizer
    if (this.resizer.show) 
    	this.resizer.draw(this.ctx);
   
    // Infine se presente disegno lo sfondo
	if (this.background) 
		this.background.draw(this.ctx);
	       
    this.dirty = false; // imposto il flag dirty a false 
  }
};

/********************************************************************************
 *  Classe Resizer
 *  Questa classe rappresenta l'ancora utilizzata nello strumento "ridimensiona"
 *******************************************************************************/
function Resizer(_x,_y) {
	  this.x1 = _x-11;
	  this.y1 = _y-11;
	  this.show = false;
	  this.ready = false;
	  this.imgObj = new Image();
	  var _this = this;
	  this.imgObj.onload = function () { _this.ready = true;};
	  this.imgObj.src = "html5wb/resizer.png";
	  this.x2 = _x +10;
	  this.y2 = _y + 10; 
	  this.width = 21;
	  this.height = 21;
	};

Resizer.prototype.draw = function(context) {
	if (this.ready)
		context.drawImage(this.imgObj,this.x1,this.y1,this.width,this.height);
};

Resizer.prototype.contains = function(mx, my) {
	if ((this.x1 <= mx) && (this.x2 >= mx) && (this.y1 <= my) && (this.y2 >= my) && this.show) return true;
	return false;
};

Resizer.prototype.move = function(dx, dy) {
	this.x1 = dx-11;
	this.y1 = dy-11;
	this.x2 = dx+10;
	this.y2 = dy+10;
};

/**************************************************************************
 * Classe Path
 * Questa Classe utilizza due vettori per tenere traccia delle coordinate 
 * utilizzate nel disegno a mano libera
 *************************************************************************/

function Path( _xv, _yv, _color, _size, _dash, _author, _id) {
  this.x1,this.x2,this.y1,this.y2; // coordinate per la bound box
  this.vx = _xv; // Vettore per le x
  this.vy = _yv; // Vettore per le y
  this.color = _color; // 
  this.author = _author;
  this.id = _id;
  this.size = _size;
  this.dash = _dash;
  this.label = "PENCIL";
  this.setBound();
}

Path.prototype.clone = function() {
  return new Path(this.vx,this.vy,this.color,this.size,this.dash,this.author,this.id);
};

// Setta la bound box
Path.prototype.setBound = function() {
  this.x1 = Math.min.apply(0,this.vx);
  this.x2 = Math.max.apply(0,this.vx);
  this.y1 = Math.min.apply(0,this.vy);
  this.y2 = Math.max.apply(0,this.vy);
};

// Aggiunge un punto al Path
// QUESTO METODO DEVE ESSERE CHIAMATO ALMENO UNA VOLTA PRIMA DEL METODO draw()
Path.prototype.addPoint = function(_x , _y) {
  this.vx.push(_x);
  this.vy.push(_y);
  this.setBound();	
};

Path.prototype.draw = function(context) {
  context.beginPath(); // apre un path
  context.moveTo(this.vx[0], this.vy[0]); // Punto iniziale
  for (var i=1;i < this.vx.length; i++) // Per il resto dei punti
	context.lineTo(this.vx[i], this.vy[i]);  // traccia una linea
  //Imposto lo stile delle linee
  context.lineWidth = this.size; 
  context.lineCap = "round";
  context.lineJoin = "round";	
  context.strokeStyle = "#" + this.color;
  context.stroke();
  context.closePath(); // Chiudo il path	
};


Path.prototype.contains = function(mx, my) {
	var oSet = this.size/2 + 5;
	for (var i=0;i < this.vx.length; i++) 
		if (mx<this.vx[i]+oSet && mx>=this.vx[i]-oSet && my<=this.vy[i]+oSet && my>=this.vy[i]-oSet)
			return true;
  return false;
};

Path.prototype.move = function(dx, dy) {
	this.x1 = this.x1+dx;
	this.y1 = this.y1+dy;
	this.x2 = this.x2+dx;
	this.y2 = this.y2+dy;
	for (var i=0;i < this.vx.length; i++) {
		this.vx[i] = this.vx[i]+dx;
		this.vy[i] = this.vy[i]+dy;
	}
};

Path.prototype.getXML = function() {
	return "<figure object=\""+this.label+"\">"
				+"<vector_X>"+this.vx+"</vector_X>"
				+"<vector_Y>"+this.vy+"</vector_Y>"
				+"</figure>";
};


/************************************************************************
 * Classe Rubber
 * Questa Classe utilizza due vettori per tenere traccia delle coordinate 
 * utilizzate per lo strumendo gomma.
 * Inoltre nel metodo draw vengono utilizzate le operazioni di composite
 * per ricreare l'effetto corretto della gomma da cancellare
 ************************************************************************/

function Rubber( _xv, _yv, _color, _size, _dash, _author, _id) {
  this.vx = _xv; // Vettore per le x
  this.vy = _yv; // Vettore per le y
  this.color = _color;
  this.author = _author;
  this.id = _id;
  this.size = _size;
  this.dash = _dash;
  this.label = "RUBBER";
}

Rubber.prototype.clone = function() {
  return new Rubber(this.vx,this.vy,this.color,this.size,this.dash,this.author,this.id);
};

// Aggiunge un punto al Rubber
// QUESTO METODO DEVE ESSERE CHIAMATO ALMENO UNA VOLTA PRIMA DEL METODO draw()
Rubber.prototype.addPoint = function(_x , _y) {
  this.vx.push(_x);
  this.vy.push(_y);
//  this.setBound();	
};

Rubber.prototype.draw = function(context) {
  context.globalCompositeOperation = 'destination-out'; // prima di disegnare imposto la composizione con 'destination-out' che praticamente cancella i pixel occupati dalla figura Rubber
  context.beginPath(); // apre un path
  context.moveTo(this.vx[0], this.vy[0]); // Punto iniziale
  for (var i=1;i < this.vx.length; i++) // Per il resto dei punti
	context.lineTo(this.vx[i], this.vy[i]);  // traccia una linea
  //Imposto lo stile delle linee
  context.lineWidth = this.size; 
  context.lineCap = "round";
  context.lineJoin = "round";
  context.strokeStyle = "#" + this.color;
  context.stroke();
  context.closePath(); // Chiudo il path	
  context.globalCompositeOperation = 'source-over'; // reimposto la composizione ai valori di default

};

// Stub
Rubber.prototype.contains = function(mx, my) { return false; };
Rubber.prototype.move = function(dx, dy) { return false; };

Rubber.prototype.getXML = function() {
	return "<figure object=\""+this.label+"\">"
				+"<vector_X>"+this.vx+"</vector_X>"
				+"<vector_Y>"+this.vy+"</vector_Y>"
				+"</figure>";
};

/*******************************************************************************************
 * Classe Line
 * Questa classe rappresenta una linea, se utilizziamo lo stile tratteggiato per le linee
 * utilizziamo la nostra funzione dashedLineTo
 * 
 *  TODO : Migliorare la funzione per selezionare (contain)
 ******************************************************************************************/

function Line(_x1, _y1, _x2, _y2, _color, _size, _dash, _author, _id) {
  this.x1 = _x1;
  this.y1 = _y1;
  this.x2 = _x2;
  this.y2 = _y2;
  this.color = _color;
  this.author = _author;
  this.id = _id;
  this.size = _size;
  this.dash = _dash;
  this.label = "LINE";
}

Line.prototype.clone = function() {
  return new Line(this.x1,this.y1,this.x2,this.y2,this.color,this.size,this.dash,this.author,this.id);
};

Line.prototype.draw = function(context) {
  context.beginPath();
  context.moveTo(this.x1, this.y1);
  if (this.dash) 
	context.dashedLineTo(this.x1, this.y1,this.x2, this.y2,[ this.size*2, this.size*2]);
  else context.lineTo(this.x2, this.y2);
  //Imposto lo stile delle linee
  context.lineWidth = this.size; 
  context.lineCap = "round";
  context.strokeStyle = "#" + this.color;
  context.stroke();
  context.closePath(); // Chiudo il path	
};

Line.prototype.resize = function(_x,_y) {
	this.x2 = _x;
	this.y2 = _y;
};

Line.prototype.contains = function(mx, my) {
  var inx = false;
  var iny = false;
  var oSet = this.size/2;
  
  if (this.x1 <= this.x2 && (this.x1-oSet <= mx) && (this.x2+oSet >= mx)) inx = true;
  else if (this.x2 < this.x1 && (this.x2-oSet <= mx) && (this.x1+oSet >= mx)) inx = true;
  
  if (this.y1 <= this.y2 && (this.y1-oSet <= my) && (this.y2+oSet >= my)) iny = true;
  else if (this.y2 < this.y1 && (this.y2-oSet <= my) && (this.y1+oSet >= my)) iny = true;

  if (inx && iny) {
	if (this.x1!=this.x2 && this.y1!=this.y2) {
		for (var i = mx-this.size/2;i<=mx+this.size/2;i++)
			for (var e = my-this.size/2;e<=my+this.size/2;e++) {
				testy = ((e - this.y1)/(this.y2-this.y1));
				testx = ((i - this.x1)/(this.x2-this.x1));
				if (testy.toFixed(2)==testx.toFixed(2)) 
					return true;
			};
	} else {
		if (this.x1+oSet>=mx && this.x1-oSet<=mx) return true;
		else if (this.y1+oSet>=my && this.y1-oSet<=my) return true;
	};
  }
  return false;	
};

Line.prototype.move = function(dx, dy) {
	this.x1 = this.x1+dx;
	this.y1 = this.y1+dy;
	this.x2 = this.x2+dx;
	this.y2 = this.y2+dy;
};

Line.prototype.getXML = function() {
	
	return "<figure object=\""+this.label+"\">"
			+"<x1>"+this.x1+"</x1>"
			+"<y1>"+this.y1+"</y1>"
			+"<x2>"+this.x2+"</x2>"
			+"<y2>"+this.y2+"</y2>"
			+"</figure>";
};

/***********************************************************************************************
 * Classe Arrow
 * Questa classe rappresenta una freccia, se utilizziamo lo stile tratteggiato per le linee
 * utilizziamo la nostra funzione dashedLineTo unicamente per la linea e non per la testa della
 * freccia
 * 
 *  TODO : Migliorare la funzione per selezionare (contain)
 **********************************************************************************************/

function Arrow(_x1, _y1, _x2, _y2, _color, _size, _dash, _author, _id) {
  this.x1 = _x1;
  this.y1 = _y1;
  this.x2 = _x2;
  this.y2 = _y2;
  this.color = _color;
  this.author = _author;
  this.id = _id;
  this.size = _size;
  this.dash = _dash;
  this.label = "ARROW";
}


Arrow.prototype.clone = function() {
  return new Arrow(this.x1,this.y1,this.x2,this.y2,this.color,this.size,this.dash,this.author,this.id);
};

Arrow.prototype.draw = function(context) {
  
  var angle = Math.PI/8; // angolo della freccia
  var lineangle=Math.atan2(this.y2-this.y1,this.x2-this.x1); // angolo della linea
  var d = 15; // altezza della testa della freccia
  var h=Math.abs(d/Math.cos(angle)); // lunghezza di un lato della testa della freccia
  
  // Calcolo i vertici del triangolo
  var angle1=lineangle+Math.PI+angle;
  var topx=this.x2+Math.cos(angle1)*h;
  var topy=this.y2+Math.sin(angle1)*h;
  var angle2=lineangle+Math.PI-angle;
  var botx=this.x2+Math.cos(angle2)*h;
  var boty=this.y2+Math.sin(angle2)*h;  

  context.beginPath(); // inizio il path
  context.moveTo(this.x1, this.y1); // mi muovo al primo punto 
  if (this.dash) // se tratteggiato
	context.dashedLineTo(this.x1, this.y1,this.x2, this.y2,[this.size*2,this.size*2]); // linea tratteggiata fino al secondo punto
  else // altrimenti
	context.lineTo(this.x2, this.y2); // linea al secondo punto

  context.lineWidth = this.size;	
  context.strokeStyle = "#" + this.color;
  context.lineCap = "round";
  context.stroke();

  context.beginPath(); // inizio il path	
  context.moveTo(this.x2,this.y2); // linea fino al vertice alto
  context.lineTo(botx,boty); // linea fino al vertice basso
  context.lineTo(topx,topy); // linea per chiudere il path
  context.closePath();	

  // imposto lo stile delle linee
  context.fillStyle = "#" + this.color;
  context.fill(); 
  context.stroke();


};


Arrow.prototype.resize = function(_x,_y) {
	this.x2 = _x;
	this.y2 = _y;
};


Arrow.prototype.contains = function(mx, my) {
  var inx = false;
  var iny = false;
  var oSet = this.size/2;
  
  if (this.x1 <= this.x2 && (this.x1-oSet <= mx) && (this.x2+oSet >= mx)) inx = true;
  else if (this.x2 < this.x1 && (this.x2-oSet <= mx) && (this.x1+oSet >= mx)) inx = true;
  
  if (this.y1 <= this.y2 && (this.y1-oSet <= my) && (this.y2+oSet >= my)) iny = true;
  else if (this.y2 < this.y1 && (this.y2-oSet <= my) && (this.y1+oSet >= my)) iny = true;

  if (inx && iny) {
	if (this.x1!=this.x2 && this.y1!=this.y2) {
		for (var i = mx-this.size/2;i<=mx+this.size/2;i++)
			for (var e = my-this.size/2;e<=my+this.size/2;e++) {
				testy = ((e - this.y1)/(this.y2-this.y1));
				testx = ((i - this.x1)/(this.x2-this.x1));
				if (testy.toFixed(2)==testx.toFixed(2)) 
					return true;
			};
	} else {
		if (this.x1+oSet>=mx && this.x1-oSet<=mx) return true;
		else if (this.y1+oSet>=my && this.y1-oSet<=my) return true;
	};
  }
  return false;	
};

Arrow.prototype.move = function(dx, dy) {
	this.x1 = this.x1+dx;
	this.y1 = this.y1+dy;
	this.x2 = this.x2+dx;
	this.y2 = this.y2+dy;
};


Arrow.prototype.getXML = function() {
	return "<figure object=\""+this.label+"\">"
			+"<x1>"+this.x1+"</x1>"
			+"<y1>"+this.y1+"</y1>"
			+"<x2>"+this.x2+"</x2>"
			+"<y2>"+this.y2+"</y2>"
			+"</figure>";
};

/******************************************************************************************************
 * Classe Rect
 * Questa classe rapppresenta un rettangolo, nel caso il rettangolo è non utilizza linee tratteggiate
 * per disegnarlo utilizziamo le funzioni messe a disposizione nativamente dal canvas (rect) altrimenti
 * per disegnare il rettangolo utilizziamo la nostra funzione dashedLineTo per disegnare quattro linee
 *******************************************************************************************************/

function Rect(_x1, _y1, _x2, _y2, _fill, _color, _size, _dash, _author, _id) {
  this.x1 = _x1;
  this.y1 = _y1;
  this.x2 = _x2;
  this.y2 = _y2;
  this.fill = _fill;
  this.color = _color;
  this.author = _author;
  this.id = _id;
  this.size = _size;
  this.dash = _dash;
  this.label = "RECT";
}

Rect.prototype.clone = function() {
  return new Rect(this.x1,this.y1,this.x2,this.y2,this.fill,this.color,this.size,this.dash,this.author,this.id);
};

Rect.prototype.draw = function(context) {
  context.beginPath();
  
  // Stiamo usando il tratteggio 
  if (this.dash && !this.fill) {
    context.dashedLineTo(this.x1, this.y1,this.x1, this.y2,[ this.size*2, this.size*2]);
    context.dashedLineTo(this.x1, this.y2,this.x2, this.y2,[ this.size*2, this.size*2]);
    context.dashedLineTo(this.x1, this.y1,this.x2, this.y1,[ this.size*2, this.size*2]);
    context.dashedLineTo(this.x2, this.y2,this.x2, this.y1,[ this.size*2, this.size*2]);
  } else { // altrimenti
    context.rect(this.x1, this.y1, this.x2 - this.x1, this.y2 - this.y1);
	if (this.fill) {  // il controllo è dentro l'else perche non è possibile che la figura sia sia piena che tratteggiata
		context.fillStyle = "#" + this.color;
		context.fill();
	}
  }
  
  context.lineWidth = this.size;
  context.strokeStyle = "#" + this.color;
  context.lineCap = "round";
  context.stroke();
  context.closePath();	


};

Rect.prototype.resize = function(_x,_y) {
	this.x2 = _x;
	this.y2 = _y;
};

Rect.prototype.contains = function(mx, my) {
  var inx = false;
  var iny = false;
  var oSet = this.size/2;
  
  if (this.x1 <= this.x2 && (this.x1-oSet <= mx) && (this.x2+oSet >= mx)) inx = true;
  else if (this.x2 < this.x1 && (this.x2-oSet <= mx) && (this.x1+oSet >= mx)) inx = true;
  
  if (this.y1 <= this.y2 && (this.y1-oSet <= my) && (this.y2+oSet >= my)) iny = true;
  else if (this.y2 < this.y1 && (this.y2-oSet <= my) && (this.y1+oSet >= my)) iny = true;

  return  (inx) && (iny);
};

Rect.prototype.move = function(dx, dy) {
	this.x1 = this.x1+dx;
	this.y1 = this.y1+dy;
	this.x2 = this.x2+dx;
	this.y2 = this.y2+dy;
};


Rect.prototype.getXML = function() {
	return "<figure object=\""+this.label+"\">"
			+"<x1>"+this.x1+"</x1>"
			+"<y1>"+this.y1+"</y1>"
			+"<x2>"+this.x2+"</x2>"
			+"<y2>"+this.y2+"</y2>"
			+"<beFill>"+this.fill+"</beFill>"
			+"</figure>";
};

/*********************************************************************************************************************
 * Classe Ellipse
 * Questa classe rappresenta un elllisse, per realizzare l'ellisse utilizzaimo due curve bezier
 * come mostrato in http://stackoverflow.com/questions/2172798/how-to-draw-an-oval-in-html5-canvas/2173084#2173084
 * (altre info su http://it.wikipedia.org/wiki/Curva_di_B%C3%A9zier)
 * Nel caso lo stile delle linee è tratteggiato allora utilizziamo la nostra funzione 
 * dashedBezierCurveTo 
 *********************************************************************************************************************/

function Ellipse(_x1, _y1, _x2, _y2, _fill, _color, _size, _dash, _author, _id) {
	this.x1 = _x1;
	this.y1 = _y1;
	this.x2 = _x2;
	this.y2 = _y2;
	this.fill = _fill;
	this.color = _color;
	this.author = _author;
	this.id = _id;
	this.size = _size;
	this.dash = _dash;
	this.label = "ELLIPSE";
}

Ellipse.prototype.clone = function() {
  return new Ellipse(this.x1,this.y1,this.x2,this.y2,this.fill,this.color,this.size,this.dash,this.author,this.id);
};

Ellipse.prototype.draw = function(context) {
  var width =  this.x2 -  this.x1;
  var height =  this.y2 -  this.y1;

  var kappa = .5522848;
  ox = (width / 2) * kappa;  // control point offset horizontal
  oy = (height / 2) * kappa; // control point offset vertical
  xm = this.x1 + width / 2;  // x-middle
  ym = this.y1 + height / 2; // y-middle

  context.beginPath();
   
  if (this.dash && !this.fill) {
	  context.dashedBezierCurveTo(this.x1, ym, this.x1, (ym - oy), (xm - ox), this.y1, xm, this.y1, [ this.size*2, this.size*2]);
	  context.dashedBezierCurveTo(xm, this.y1, (xm + ox), this.y1, this.x2, (ym - oy), this.x2, ym, [ this.size*2, this.size*2]);
	  context.dashedBezierCurveTo(this.x2, ym, this.x2, (ym + oy), (xm + ox), this.y2, xm, this.y2, [ this.size*2, this.size*2]);
	  context.dashedBezierCurveTo(xm, this.y2, (xm - ox), this.y2, this.x1, (ym + oy), this.x1, ym, [ this.size*2, this.size*2]);
  } else {
	  context.moveTo(this.x1, ym);
	  context.bezierCurveTo(this.x1, (ym - oy), (xm - ox), this.y1, xm, this.y1);
	  context.bezierCurveTo(xm + ox, this.y1, this.x2, (ym - oy),this.x2, ym);	   
	  context.bezierCurveTo(this.x2, (ym + oy), (xm + ox), this.y2, xm, this.y2);
	  context.bezierCurveTo((xm - ox), this.y2,this.x1, (ym + oy), this.x1, ym);
	  if (this.fill) { 
		  context.fillStyle = "#" + this.color;
		  context.fill();
	  };
   }
  context.lineWidth = this.size;
  context.strokeStyle = "#" + this.color;
  context.lineCap = "round";
  context.stroke();
  context.closePath();	
};

Ellipse.prototype.resize = function(_x,_y) {
	this.x2 = _x;
	this.y2 = _y;
};

Ellipse.prototype.contains = function(mx, my) {
  var c_x = (this.x1 + this.x2)/2;
  var c_y = (this.y1 + this.y2)/2;
  var oSet = this.size/2;
  var b = Math.abs(this.x1-c_x)+oSet;
  var a = Math.abs(this.y1-c_y)+oSet;
  var chk = Math.pow((mx-c_x),2) / Math.pow(b,2) + Math.pow((my - c_y),2) / Math.pow(a,2);
  return  (chk < 1);
};

Ellipse.prototype.move = function(dx, dy) {
	this.x1 = this.x1+dx;
	this.y1 = this.y1+dy;
	this.x2 = this.x2+dx;
	this.y2 = this.y2+dy;
};

Ellipse.prototype.getXML = function() {
	return "<figure object=\""+this.label+"\">"
			+"<x1>"+this.x1+"</x1>"
			+"<y1>"+this.y1+"</y1>"
			+"<x2>"+this.x2+"</x2>"
			+"<y2>"+this.y2+"</y2>"
			+"<beFill>"+this.fill+"</beFill>"
			+"</figure>";
};

/************************************************************************************
 * Classe Text
 * Questa classe rappresenta il un testo, in particolare abbiamo delle variabili
 * che indicano lo stile del testo (size, font family), inoltre la classe tiene conto
 * del carattere di "a capo" per posizionare il testo su più linee 
 ************************************************************************************/

function Text(_x1, _y1, _text, _color, _size, _family, _author, _id) {
  this.x1 = _x1;
  this.y1 = _y1;
  this.x2 = 0;
  this.y2 = 0;
  this.text = _text;
  this.color = _color;
  this.author = _author;
  this.id = _id;
  this.size = _size;
  this.family = _family;
  this.x2;
  this.y2;
  this.label = "TEXT";
}

Text.prototype.clone = function() {
  return new Text(this.x1,this.y1,this.text,this.color,this.size,this.family,this.author,this.id);
};

Text.prototype.setBound = function(context) {
	this.x2 = 0;
	this.y2 = 0;
	var textvalArr = this.text.split('\n'); // conto le righe
	for(var i = 0; i < textvalArr.length; i++) {// e per ognuna
        if (this.x1+context.measureText(textvalArr[i]).width > this.x2) this.x2 = this.x1+context.measureText(textvalArr[i]).width;
	}
	this.y2 = this.y1+this.size * textvalArr.length;
};

Text.prototype.draw = function(context) {
	context.textBaseline = 'top';
    context.font = this.size +"px "+this.family ; // imposto lo stile
    context.fillStyle = "#" + this.color;
    var textvalArr = this.text.split('\n'); // conto le righe
    for(var i = 0; i < textvalArr.length; i++){ // e per ognuna
        context.fillText(textvalArr[i], this.x1, this.y1 + i * this.size); // la disegno
    }
	this.setBound(context);

};

Text.prototype.contains = function(mx, my) {
  var inx = false;
  var iny = false;
  
  if (this.x1 < this.x2 && (this.x1 <= mx) && (this.x2 >= mx)) inx = true;
  else if (this.x2 < this.x1 && (this.x2 <= mx) && (this.x1 >= mx)) inx = true;
  
  if (this.y1 < this.y2 && (this.y1 <= my) && (this.y2 >= my)) iny = true;
  else if (this.y2 < this.y1 && (this.y2 <= my) && (this.y1 >= my)) iny = true;

  return  (inx) && (iny);
  
};

Text.prototype.move = function(dx, dy) {
	this.x1 = this.x1+dx;
	this.y1 = this.y1+dy;
	this.x2 = this.x2+dx;
	this.y2 = this.y2+dy;
};

Text.prototype.getXML = function() {
	return "<figure object=\""+this.label+"\">"
		+"<x>"+this.x1+"</x>"
		+"<y>"+this.y1+"</y>"
		+"<text><![CDATA["+this.text+"]]></text>"
		+"</figure>";
};

/******************************************************************
 * Classe laser
 * Questa classe rappresenta il puntatore laser.
 * Viene disegnato sul canvas un cerchio tramite il metodo arc()
 * e viene aggiunto un gradiente per dare l'effetto puntatore laser
 ******************************************************************/

function Laser(_x, _y, _color, _size, _author, _id) {
	this.x = _x;
	this.y = _y;
	this.author = _author;
	this.id = _id;
	this.size = _size;
	this.color = _color;
	this.label = "LASER";
};

Laser.prototype.draw = function(context) {
	context.beginPath();
	context.arc(this.x, this.y, this.size*2, 0, 2 * Math.PI, false);
	var grd = context.createRadialGradient(this.x, this.y, 0, this.x, this.y,  this.size); // creo un gradiente radiale
	grd.addColorStop(0, hex2rgb(this.color,0.5)); // il primo colore è con alpha 0.5
	grd.addColorStop(1, hex2rgb(this.color,0)); // il secondo colore è con alpha a 0
	context.fillStyle = grd;
	context.fill();
	context.closePath();
};

// Stub
Laser.prototype.contains = function(mx, my) { return false; };

Laser.prototype.getXML = function() {
	return "<figure object=\""+this.label+"\">"
			+"<x>"+this.x+"</x>"
			+"<y>"+this.y+"</y>"
			+"<radius>"+this.size+"</radius>"
			+"</figure>";
};


/******************************************************************************
 * Classe Highlight
 * Praticamente uguale alla classe rect tranne che per l'opacità sempre settata
 ******************************************************************************/

function Highlight(_x1, _y1, _x2, _y2, _fill, _color, _size, _dash, _author, _id) {
  this.x1 = _x1;
  this.y1 = _y1;
  this.x2 = _x2;
  this.y2 = _y2;
  this.fill = _fill;
  this.color = _color;
  this.author = _author;
  this.id = _id;
  this.size = _size;
  this.dash = _dash;
  this.label = "HIGHLIGHT";
}

Highlight.prototype.clone = function() {
  return new Highlight(this.x1,this.y1,this.x2,this.y2,this.fill,this.color,this.size,this.dash,this.author,this.id);
};

Highlight.prototype.draw = function(context) {
  context.beginPath();
  context.rect(this.x1, this.y1, this.x2 - this.x1, this.y2 - this.y1);
  context.fillStyle = hex2rgb("#"+this.color,0.3);
  context.fill();
  context.lineWidth = this.size;
  context.closePath();	
};

Highlight.prototype.resize = function(_x,_y) {
	this.x2 = _x;
	this.y2 = _y;
};

Highlight.prototype.contains = function(mx, my) {
  var inx = false;
  var iny = false;
  
  if (this.x1 < this.x2 && (this.x1 <= mx) && (this.x2 >= mx)) inx = true;
  else if (this.x2 < this.x1 && (this.x2 <= mx) && (this.x1 >= mx)) inx = true;
  
  if (this.y1 < this.y2 && (this.y1 <= my) && (this.y2 >= my)) iny = true;
  else if (this.y2 < this.y1 && (this.y2 <= my) && (this.y1 >= my)) iny = true;

  return  (inx) && (iny);
};

Highlight.prototype.move = function(dx, dy) {
	this.x1 = this.x1+dx;
	this.y1 = this.y1+dy;
	this.x2 = this.x2+dx;
	this.y2 = this.y2+dy;
};

Highlight.prototype.getXML = function() {
	return "<figure object=\""+this.label+"\">"
			+"<x1>"+this.x1+"</x1>"
			+"<y1>"+this.y1+"</y1>"
			+"<x2>"+this.x2+"</x2>"
			+"<y2>"+this.y2+"</y2>"
			+"<beFill>"+this.fill+"</beFill>"
			+"</figure>";
};


/************************************************************************************
 * Classe Img
 * Questa classe rappresenta un immagine e utilizza un oggetto Image() di javascript
 ************************************************************************************/


function Img(_x, _y, _type, _author, _id) {
  this.x1 = _x;
  this.y1 = _y;
  this.author = _author;
  this.id = _id;
  this.ready = false; // flag per capire se l'immagine � correttamente caricata in memoria
  this.resized = false; // flag per sapere se le dimensioni dell'immagine non sono quelle originali
  this.imgObj;
  this.imgB64;
  this.x2;
  this.y2;
  this.width;
  this.height;
  this.label = "CLIP_ART";
  this.type = _type;
}

Img.prototype.resize = function(_x,_y) {
	this.resized = true;
	this.x2 = _x;
	this.y2 = _y;
	this.width = _x - this.x1;
	this.height = _y - this.y1;
};

Img.prototype.clone = function() {
	var tmp = new Img(this.x1,this.y1,this.type,this.author,this.id);
	tmp.width = this.width;
	tmp.height = this.height;
	tmp.x2 = this.x2;
	tmp.y2 = this.y2;
	tmp.imgObj = this.imgObj;
	tmp.imgB64 = this.imgB64;
  return tmp; 
};

Img.prototype.draw = function(context) {
	if (this.ready) 
		context.drawImage(this.imgObj,this.x1,this.y1,this.width,this.height);
};

Img.prototype.setImg = function(img,b64) {
	 this.imgObj = img;
	 this.imgB64 = b64;
	 if (!this.resized) {
		 this.width = img.width;
		 this.height = img.height;
		 this.x2 = this.x1 +  img.width;
	 	 this.y2 = this.y1 +  img.height;
	 }
	 this.ready = true;
	
};

Img.prototype.contains = function(mx, my) {
var inx = false;
  var iny = false;
  
  if (this.x1 < this.x2 && (this.x1 <= mx) && (this.x2 >= mx)) inx = true;
  else if (this.x2 < this.x1 && (this.x2 <= mx) && (this.x1 >= mx)) inx = true;
  
  if (this.y1 < this.y2 && (this.y1 <= my) && (this.y2 >= my)) iny = true;
  else if (this.y2 < this.y1 && (this.y2 <= my) && (this.y1 >= my)) iny = true;

  return  (inx) && (iny);
  
};

Img.prototype.move = function(dx, dy) {
	this.x1 = this.x1+dx;
	this.y1 = this.y1+dy;
	this.x2 = this.x2+dx;
	this.y2 = this.y2+dy;
};

Img.prototype.getXML = function() {
	return "<figure object=\""+this.label+"\">"
			+"<x>"+this.x1+"</x>"
			+"<y>"+this.y1+"</y>"
			+"<x2>"+this.x2+"</x2>"
			+"<y2>"+this.y2+"</y2>"
			+"<image>"+this.imgB64+"</image>"
			+"<file_type>"+this.type+"</file_type>"
			+"</figure>";
};

/************************************************************************************
 * Classe BGImg
 * Questa classe rappresenta uno sfondo e utilizza un oggetto Image() di javascript
 ************************************************************************************/

function BGImg(_x, _y, _type, _author, _id) {
  this.x1 = _x;
  this.y1 = _y;
  this.author = _author;
  this.id = _id;
  this.ready = false; // flag per capire se l'immagine è correttamente caricata in memoria
  this.imgObj;
  this.imgB64;
  this.x2;
  this.y2;
  this.width;
  this.height;
  this.label = "BG_IMAGE";
  this.type = _type;
  
};

BGImg.prototype.draw = function(context) {
	if (this.ready) {
		context.globalCompositeOperation = 'destination-over'; // Setto la composizione per disegnare sotto le figure già presenti
		context.drawImage(this.imgObj,this.x1,this.y1,this.width,this.height);
		context.globalCompositeOperation = 'source-over'; // Setto la composizione al valore di default

	}
};

BGImg.prototype.setImg = function(_x, _y, _w, _h, img, b64) {
	 this.x1 = _x;
	 this.y1 = _y;
	 this.imgObj = img;
	 this.width = _w;
	 this.height = _h;
	 this.imgB64 = b64;
	 this.x2 = this.x1 +  _w;
	 this.y2 = this.y1 +  _h;
	 this.ready = true;
};

// Stub
BGImg.prototype.contains = function(mx, my) { return false; };
BGImg.prototype.move = function(dx, dy) { return false; };
BGImg.prototype.clone = function() { return null; };

BGImg.prototype.getXML = function() {
	return "<figure object=\""+this.label+"\">"
			+"<image>"+this.imgB64+"</image>"
			+"<file_type>"+this.type+"</file_type>"
			+"</figure>";
};
