/************************************************************************************
 * 
 * wb_utils.js
 * 
 * Raccolta di funzioni utilizzate dalla WebApp
 * 
 *  @author Claudio Perrotta <c.perrotta@studenti.unina.it>,<cl.perrotta@gmail.com>
 *
 ************************************************************************************/

// hex2rgb : Converte i colori da esadecimale a ARGB o RGB 
function hex2rgb(hex, opacity) {
	var rgb = hex.replace('#', '').match(/(.{2})/g); // Elimino il simbolo # se presente
    
	// effettuo il parsing delle componenti
	var i = 3; 
	while (i--) 
		rgb[i] = parseInt(rgb[i], 16);
 
	if (typeof opacity == 'undefined') // controllo se in input è presente un valore per l'opacità 
		return 'rgb(' + rgb.join(', ') + ')';
	return 'rgba(' + rgb.join(', ') + ', ' + opacity + ')';
};

// genericID : crea un semplice id in maniera random
// TODO Eseguire test per vedere se le possibili collissioni inficiano l'uso della funzione
function genericID() {
	return Math.floor(Math.random() * 0x10000 ).toString(16);
}

// loadTheme : Sulla base degli input effettua il caricamento dei css del relativo tema e stile
function loadTheme( tema , ver) {
	// Controllo gli input
	if (tema == "")
		tema = "light"; 		
	if (ver == "") 
		ver = 1;
	
	// effettuo il load
	$('head').append('<link rel="stylesheet" type="text/css" href="css/'+tema+'/jquery-ui-1.9.0.custom.css"/>');
	$('head').append('<link rel="stylesheet" type="text/css" href="css/'+tema+'/style'+ver+'.css"/>');
	$('head').append('<link rel="stylesheet" type="text/css" href="css/'+tema+'/common.css"/>');
	
}

// getParameterByName : Individua nella query string il paramatro sulla base del nome e resituisce il valore associato se presente altrimenti
// restituisce una stringa vuota
// (es. querystring nome1=valore1&nome2=valore2)
function getParameterByName(name) {
	
	name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]"); // pulisco l'input
	
	// Creo l'espressione regolare
	var regexS = "[\\?&]" + name + "=([^&#]*)"; 
	var regex = new RegExp(regexS);
	
	// cerco nell'url
	var results = regex.exec(window.location.search);
	
	
	if(results == null) // se non trovo il parametro
		return ""; // restituisco la stringa vuota
	else // altrimenti
		return decodeURIComponent(results[1].replace(/\+/g, " ")); // restituisco il valore
}

// secondToHHMMSS : converte una stringa che rappresenta i secondi in una stringa del tipo HH:MM:SS 
// fonte : http://stackoverflow.com/a/6313008
function secondsToHHMMSS(secs) {
    sec_numb    = parseInt(secs); // faccio il parsing della stringa
    var hours   = Math.floor(sec_numb / 3600); // estraggo le ore
    var minutes = Math.floor((sec_numb - (hours * 3600)) / 60); // estraggo i minuti
    var seconds = sec_numb - (hours * 3600) - (minutes * 60); // estraggo i secondi

    // Controlli per la giusta formattazione
    if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}
   
    return hours+':'+minutes+':'+seconds; // restituisco il risultato
};